package org.uber.components;

import org.apache.log4j.Logger;
import org.uber.backend.UberService;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutListener;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.themes.Reindeer;

public class UberGrid<T> {

	private final static Logger logger = Logger.getLogger(UberGrid.class);

	private UberService<T> service;
	private BeanItemContainer<T> container;
	private Grid grid;
	private int currentPage = 1;
	private double rowsPerPage = 10.0;
	private VerticalLayout vLayout;
	private TextField currentPageTF;
	private TextField searchInput;
	private Button firstButton;
	private Button previousButton;
	private Button nextButton;
	private Button lastButton;
	private Label pagesLabel;

	public UberGrid(UberService<T> service, Class<T> objectClass) {
		this.service = service;
		vLayout = new VerticalLayout();
		currentPageTF = new TextField();
		container = new BeanItemContainer<>(objectClass);

		container.addAll(service.getLimitedList(0, (int) rowsPerPage));

		GeneratedPropertyContainer gpc = new GeneratedPropertyContainer(container);

		gpc.addGeneratedProperty("delete", new PropertyValueGenerator<String>() {
			private static final long serialVersionUID = 1852320774109896695L;

			@Override
			public String getValue(Item item, Object itemId, Object propertyId) {
				return "Usuń";
			}

			@Override
			public Class<String> getType() {
				return String.class;
			}
		});

		grid = new Grid(gpc);
		grid.setId("table");
		grid.getColumn("delete").setRenderer(new ButtonRenderer(e -> {
			grid.getContainerDataSource().removeItem(e.getItemId());
			service.delete(e.getItemId());
			refreshGrid();
		}));

		grid.setSizeFull();
		grid.setHeightMode(HeightMode.ROW);
		grid.setHeightByRows(rowsPerPage);
		grid.setColumnReorderingAllowed(true);
		vLayout.addComponent(createSearchLayout());
		vLayout.addComponent(grid);
		vLayout.addComponent(createControls());
	}

	public void refreshGrid() {
		logger.info(container.removeAllItems());
		int pages = countPages();
		if (currentPage > pages) {
			currentPage = pages;
			currentPageTF.setValue(Integer.toString(currentPage));
			pagesLabel.setValue(Integer.toString(currentPage));
		}
		if (Integer.parseInt(pagesLabel.getValue()) != pages) {
			pagesLabel.setValue(Integer.toString(pages));
		}
		container.addAll(service.getLimitedList((int) ((currentPage - 1) * rowsPerPage), (int) rowsPerPage));
		setButtonsEnabled();
	}

	public void setRowsPerPage(int rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
		grid.setHeightByRows(rowsPerPage);
	}

	public VerticalLayout getFullGrid() {
		return vLayout;
	}

	public Grid getGrid() {
		return grid;
	}

	public void setItemClickListener(ItemClickListener itemClickListener) {
		grid.addItemClickListener(itemClickListener);
	}

	public void setTextFieldInputPrompt(String inputPrompt) {
		searchInput.setInputPrompt(inputPrompt);
	}

	private HorizontalLayout createControls() {
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		ComboBox rowsPerPageCB = createRowsPerPageCB();

		firstButton = createFirstButton();
		previousButton = createPreviousButton();
		nextButton = createNextButton();
		lastButton = createLastButton();

		firstButton.setEnabled(false);
		previousButton.setEnabled(false);

		if (countPages() == 1) {
			nextButton.setEnabled(false);
			lastButton.setEnabled(false);
		}

		currentPageTF.setValue(Integer.toString(currentPage));
		currentPageTF.setWidth("30px");

		currentPageTF.addTextChangeListener(e -> {
			int value = Integer.parseInt(e.getText());
			if (value >= 1 && value <= countPages()) {
				currentPage = value;
				logger.info(container.removeAllItems());
				container.addAll(service.getLimitedList((int) ((currentPage - 1) * rowsPerPage), (int) rowsPerPage));
				currentPageTF.setValue(Integer.toString(currentPage));
				setButtonsEnabled();
			} else {
				currentPageTF.clear();
				currentPageTF.setValue(Integer.toString(currentPage));
				Notification.show("Wrong input!");
			}
		});

		Label pageLabel = new Label("Strona:");
		Label label = new Label("/");
		pagesLabel = new Label(Integer.toString(countPages()));
		horizontalLayout.addComponent(firstButton);
		horizontalLayout.addComponent(previousButton);
		horizontalLayout.addComponent(pageLabel);
		horizontalLayout.setComponentAlignment(pageLabel, Alignment.MIDDLE_CENTER);
		horizontalLayout.addComponent(currentPageTF);
		horizontalLayout.setComponentAlignment(currentPageTF, Alignment.MIDDLE_CENTER);
		horizontalLayout.addComponent(label);
		horizontalLayout.setComponentAlignment(label, Alignment.MIDDLE_CENTER);
		horizontalLayout.addComponent(pagesLabel);
		horizontalLayout.setComponentAlignment(pagesLabel, Alignment.MIDDLE_CENTER);
		horizontalLayout.addComponent(nextButton);
		horizontalLayout.addComponent(lastButton);
		horizontalLayout.addComponent(rowsPerPageCB);
		horizontalLayout.setComponentAlignment(rowsPerPageCB, Alignment.MIDDLE_LEFT);
		return horizontalLayout;
	}

	private ComboBox createRowsPerPageCB() {
		ComboBox cb = new ComboBox();
		cb.setWidth("75px");

		for (int i = 5; i <= 20; i += 5) {
			cb.addItem(i);
		}
		
		cb.addItem("All");
		cb.select(10);
		cb.setNullSelectionAllowed(false);
		
		cb.addValueChangeListener(e -> {
			if (e.getProperty().getValue().toString().equals("All")) {
				rowsPerPage = getNumberOfRows();
			} else {
				rowsPerPage = Integer.parseInt(e.getProperty().getValue().toString());
			}
			refreshGrid();
		});
		return cb;
	}

	private HorizontalLayout createSearchLayout() {
		HorizontalLayout layout = new HorizontalLayout();
		Label label = new Label("Szukaj: ");
		Label separator = new Label();
		separator.setWidth("10px");
		searchInput = new TextField();
		searchInput.setInputPrompt("Wpisz tekst");

		searchInput.addShortcutListener(new ShortcutListener("Clear", KeyCode.ESCAPE, null) {
			private static final long serialVersionUID = -4392067928326822035L;

			@Override
			public void handleAction(Object sender, Object target) {
				searchInput.clear();
				clearContainerAfterSearch();
			}
		});

		searchInput.addTextChangeListener(e -> {
			if (e.getText().length() >= 3) {
				logger.info(container.removeAllItems());
				container.addAll(service.search(e.getText()));
				currentPage = 1;
				currentPageTF.setValue(Integer.toString(currentPage));
				pagesLabel.setValue("1");
				disableAllButtons();
			}
			if (e.getText().isEmpty()) {
				clearContainerAfterSearch();
			}
		});

		layout.addComponent(label);
		layout.setComponentAlignment(label, Alignment.MIDDLE_LEFT);
		layout.addComponent(separator);
		layout.addComponent(searchInput);
		return layout;
	}

	private void clearContainerAfterSearch() {
		logger.info(container.removeAllItems());
		container.addAll(service.getLimitedList((int) ((currentPage - 1) * rowsPerPage), (int) rowsPerPage));
		pagesLabel.setValue(Integer.toString(countPages()));
		setButtonsEnabled();
	}

	private void disableAllButtons() {
		firstButton.setEnabled(false);
		previousButton.setEnabled(false);
		nextButton.setEnabled(false);
		lastButton.setEnabled(false);
	}

	private Button createFirstButton() {
		Button button = new Button("<<", e -> {
			logger.info(container.removeAllItems());
			container.addAll(service.getLimitedList(0, (int) rowsPerPage));
			currentPage = 1;
			currentPageTF.setValue(Integer.toString(currentPage));
			setButtonsEnabled();
		});
		button.setStyleName(Reindeer.BUTTON_LINK);
		return button;
	}

	private Button createPreviousButton() {
		Button button = new Button("<", e -> {
			logger.info(container.removeAllItems());
			currentPage--;
			container.addAll(service.getLimitedList((int) ((currentPage - 1) * rowsPerPage), (int) rowsPerPage));
			currentPageTF.setValue(Integer.toString(currentPage));
			setButtonsEnabled();
		});
		button.setStyleName(Reindeer.BUTTON_LINK);
		return button;
	}

	private Button createNextButton() {
		Button button = new Button(">", e -> {
			logger.info(container.removeAllItems());
			container.addAll(service.getLimitedList((int) (currentPage * rowsPerPage), (int) rowsPerPage));
			currentPage++;
			currentPageTF.setValue(Integer.toString(currentPage));
			setButtonsEnabled();
		});
		button.setStyleName(Reindeer.BUTTON_LINK);
		return button;
	}

	private Button createLastButton() {
		Button button = new Button(">>", e -> {
			int pages = countPages();
			logger.info(container.removeAllItems());
			container.addAll(service.getLimitedList((int) ((pages - 1) * rowsPerPage), (int) rowsPerPage));
			currentPage = pages;
			currentPageTF.setValue(Integer.toString(currentPage));
			setButtonsEnabled();
		});
		button.setStyleName(Reindeer.BUTTON_LINK);
		return button;
	}

	private void setButtonsEnabled() {
		int pages = countPages();
		if (currentPage == 1) {
			firstButton.setEnabled(false);
			previousButton.setEnabled(false);
			nextButton.setEnabled(true);
			lastButton.setEnabled(true);
		}
		if (currentPage == pages) {
			firstButton.setEnabled(true);
			previousButton.setEnabled(true);
			nextButton.setEnabled(false);
			lastButton.setEnabled(false);
		}
		if (currentPage == 1 && currentPage == pages) {
			firstButton.setEnabled(false);
			previousButton.setEnabled(false);
			nextButton.setEnabled(false);
			lastButton.setEnabled(false);
		}
		if (currentPage != pages && !nextButton.isEnabled() && !lastButton.isEnabled()) {
			nextButton.setEnabled(true);
			lastButton.setEnabled(true);
		}
		if (currentPage != 1 && !firstButton.isEnabled() && !previousButton.isEnabled()) {
			firstButton.setEnabled(true);
			previousButton.setEnabled(true);
		}
	}

	private int getNumberOfRows() {
		return service.getNumberOfRows();
	}

	private int countPages() {
		return (int) Math.ceil(getNumberOfRows() / rowsPerPage);
	}
}
