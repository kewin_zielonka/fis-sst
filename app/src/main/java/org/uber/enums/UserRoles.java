package org.uber.enums;

public enum UserRoles {
	ADMIN(1,"administrator"), MANAGER(2,"manager"), SEKRETARKA(3,"sekretarka");
	private Integer code;
	private String name;

	private UserRoles(Integer code,String name) {
		this.setCode(code);
		this.setName(name);
	}

	public Integer getCode() {
		return code;
	}

	private void setCode(Integer code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
