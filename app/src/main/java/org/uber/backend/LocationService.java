package org.uber.backend;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;

import org.apache.log4j.Logger;
import org.uber.backend.dao.DaoUber;
import org.uber.backend.dao.implement.DaoUberImpl;
import org.uber.backend.models.LocationEntity;
import org.uber.views.TestUI;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.vaadin.ui.Notification;

@ApplicationScoped
public class LocationService implements UberService<LocationEntity> {
	private DaoUber uberDao;

	private final static Logger logger = Logger.getLogger(LocationService.class);
	private GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyDhqlyMFnduYJEcaIu_XTtgyr_mEHDavTA");

	public List<LocationEntity> getLocationList() {
		uberDao =  new DaoUberImpl();

		return uberDao.getLocations();
	}

	public void saveLocations(LocationEntity locationEntity) {
		try {
			if (getLocation(locationEntity.getName()) > 0) {
				Notification.show("Lokalizacja o padanej nazwie juz istnieje");

			} else {
				addresToCoordinate(locationEntity);
				uberDao.saveLocation(locationEntity);
			}

		} catch (Exception e) {
			Notification.show("Podany adres nie istnieje");
			e.printStackTrace();

		}
	}

	public void updateLocations(LocationEntity locationEntity) {
		try {
			addresToCoordinate(locationEntity);
			uberDao.updateLocation(locationEntity);
		} catch (Exception e) {
			Notification.show("Podany adres nie istnieje");
			e.printStackTrace();

		}
	}

	public LocationEntity getLocation(Long id) {
		return uberDao.getLocation(id);
	}

	public int getLocation(String name) {
		return uberDao.getLocation(name);
	}

	public LocationEntity addresToCoordinate(LocationEntity locationEntity) throws Exception {

		GeocodingResult[] results = GeocodingApi.geocode(context, locationEntity.getAddress()).await();
		locationEntity.setLongitude(results[0].geometry.location.lng);
		locationEntity.setLatitude(results[0].geometry.location.lat);

		return locationEntity;

	}

	@Override
	public int getNumberOfRows() {
		return uberDao.countLocations();
	}

	@Override
	public List<LocationEntity> getLimitedList(int first, int max) {
		return uberDao.getLimitedLocations(first, max);
	}

	@Override
	public <T> void delete(T t) {
		uberDao.deleteLocation((LocationEntity) t);

	}
	
	public void setUberDao(DaoUber uberDao) {
		this.uberDao = uberDao;
	}

	@Override
	public Set<LocationEntity> search(String phrase) {
		Set<LocationEntity> set = new HashSet<>();
		set.addAll(uberDao.searchLocByName(phrase));
		set.addAll(uberDao.searchLocByAddress(phrase));
		return set;
	}
}
