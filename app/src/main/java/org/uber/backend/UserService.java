package org.uber.backend;

import static org.picketlink.idm.model.basic.BasicModel.getRole;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.picketlink.idm.IdentityManager;
import org.picketlink.idm.PartitionManager;
import org.picketlink.idm.RelationshipManager;
import org.picketlink.idm.credential.Password;
import org.picketlink.idm.model.basic.BasicModel;
import org.picketlink.idm.model.basic.Grant;
import org.picketlink.idm.model.basic.Group;
import org.picketlink.idm.model.basic.Role;
import org.uber.backend.dao.DaoUser;
import org.uber.backend.dao.implement.DaoUserImpl;
import org.uber.backend.models.User;

@ApplicationScoped
public class UserService implements UberService<User> {
	@Inject
	private PartitionManager partitionManager;
	private IdentityManager identityManager;
	private DaoUser daoUser = new DaoUserImpl();
	private final static Logger logger = Logger.getLogger(UserService.class);

	public User getUser(String login) {
		return daoUser.getUser(login);
	}

	public List<User> getAllUser() {
		return daoUser.getAllUsers();
	}

	public String generateMD5(String text) {
		MessageDigest md = null;
		byte[] byteData = null;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(text.getBytes());
			byteData = md.digest();
		} catch (NoSuchAlgorithmException e) {
			logger.error(e);
		}
		StringBuilder sb = new StringBuilder();
		if (byteData != null) {
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		}
		logger.info("MD5 generation failde!");
		return null;
	}

	public void updateUser(User user) {
		User userDatastore = getUser(user.getLogin());
		identityManager = this.partitionManager.createIdentityManager();
		RelationshipManager relationshipManager = partitionManager.createRelationshipManager();
		if (!user.getPassword().equals(userDatastore.getPassword()))
			user.setPassword(generateMD5(user.getPassword()));

		user.setId(userDatastore.getId());
		Role role = getRole(this.identityManager, user.getRole());
		Role roleToRemove = getRole(this.identityManager, userDatastore.getRole());

		org.picketlink.idm.model.basic.User pUser = BasicModel.getUser(identityManager, user.getLogin());
		identityManager.update(pUser);

		identityManager.updateCredential(pUser, new Password(user.getPassword()));

		BasicModel.revokeRole(relationshipManager, pUser, roleToRemove);
		BasicModel.grantRole(relationshipManager, pUser, role);

		Group group = BasicModel.getGroup(identityManager, userDatastore.getRole());
		BasicModel.removeFromGroup(relationshipManager, pUser, group);
		Group newGroup = BasicModel.getGroup(identityManager, user.getRole());

		BasicModel.addToGroup(relationshipManager, pUser, newGroup);
		daoUser.updateUser(user);
	}

	public void saveUser(User user) {
		if (getUser(user.getLogin()) != null) {
			updateUser(user);
		} else {
			identityManager = this.partitionManager.createIdentityManager();
			RelationshipManager relationshipManager = partitionManager.createRelationshipManager();
			user.setPassword(generateMD5(user.getPassword()));
			Role role = getRole(this.identityManager, user.getRole());
			org.picketlink.idm.model.basic.User pUser = new org.picketlink.idm.model.basic.User(user.getLogin());
			identityManager.add(pUser);
			identityManager.updateCredential(pUser, new Password(user.getPassword()));
			relationshipManager.add(new Grant(pUser, role));

			Group newGroup = BasicModel.getGroup(identityManager, user.getRole());
			BasicModel.addToGroup(relationshipManager, pUser, newGroup);
			daoUser.saveUser(user);
		}
	}

	@Override
	public int getNumberOfRows() {
		return daoUser.countGuests();
	}

	@Override
	public List<User> getLimitedList(int first, int max) {
		return daoUser.getLimitedUsersList(first, max);
	}

	@Override
	public <T> void delete(T t) {
		identityManager = this.partitionManager.createIdentityManager();
		org.picketlink.idm.model.basic.User pUser = BasicModel.getUser(identityManager, ((User) t).getLogin());
		identityManager.remove(pUser);
		daoUser.deleteUser((User) t);

	}

	@Override
	public Set<User> search(String phrase) {
		return new HashSet<>(daoUser.searchUserByLogin(phrase));
	}
}
