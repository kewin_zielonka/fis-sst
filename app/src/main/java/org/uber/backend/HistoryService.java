package org.uber.backend;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;

import org.uber.backend.dao.DaoUber;
import org.uber.backend.models.UberLogModel;

@ApplicationScoped
public class HistoryService implements UberService<UberLogModel> {
	private DaoUber uberDao;

	public List<UberLogModel> getLogList() throws IOException {
		return uberDao.getLogList();
	}

	public DaoUber getUberDao() {
		return uberDao;
	}

	@Override
	public int getNumberOfRows() {
		return uberDao.countLog();
	}

	@Override
	public List<UberLogModel> getLimitedList(int first, int max) {
		return uberDao.getLimitedLogList(first, max);
	}

	@Override
	public <T> void delete(T t) {
	}
	
	@Override
	public Set<UberLogModel> search(String phrase) {
		Set<UberLogModel> set = new HashSet<>();
		set.addAll(uberDao.searchLogByGuest(phrase));
		set.addAll(uberDao.searchLogByLocFrom(phrase));
		set.addAll(uberDao.searchLogByLocDest(phrase));
		return set;
	}

	public void setUberDao(DaoUber uberDao) {
		this.uberDao = uberDao;
	}
}
