package org.uber.backend.models;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "uber_search_log")
@Entity
public class UberLogModel implements Serializable {

	private static final long serialVersionUID = 432432432325L;

	public UberLogModel(String guest, Timestamp timestamp, String from, String destination, String product,
			String minCost, String maxCost, String integer) {
		super();
		this.guest = guest;
		this.data = timestamp;
		this.from = from;
		this.destination = destination;
		this.product = product;
		this.minCost = minCost;
		this.maxCost = maxCost;
		this.travelTime = integer;
	}

	public UberLogModel() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private long id;

	@Column(name = "guest", nullable = false, unique = false, length = 100)
	private String guest;

	@Column(name = "data", nullable = false, unique = false)
	private Timestamp data;

	@Column(name = "from_place", nullable = false, unique = false, length = 100)
	private String from;

	@Column(name = "destination", nullable = false, unique = false, length = 100)
	private String destination;

	@Column(name = "product", nullable = false, unique = false, length = 100)
	private String product;

	@Column(name = "min_cost", nullable = false, unique = false, length = 100)
	private String minCost;

	@Column(name = "max_cost", nullable = false, unique = false, length = 100)
	private String maxCost;

	@Column(name = "travel_time", nullable = false, unique = false, length = 100)
	private String travelTime;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getGuest() {
		return guest;
	}

	public void setGuest(String guest) {
		this.guest = guest;
	}

	public Timestamp getData() {
		return data;
	}

	public void setData(Timestamp data) {
		this.data = data;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getMinCost() {
		return minCost;
	}

	public void setMinCost(String minCost) {
		this.minCost = minCost;
	}

	public String getMaxCost() {
		return maxCost;
	}

	public void setMaxCost(String maxCost) {
		this.maxCost = maxCost;
	}

	public String getTravelTime() {
		return travelTime;
	}

	public void setTravelTime(String travelTime) {
		this.travelTime = travelTime;
	}

	@Override
	public String toString() {
		return "UberLogModel [id=" + id + ", guest=" + guest + ", data=" + data + ", from=" + from + ", destination="
				+ destination + ", product=" + product + ", minCost=" + minCost + ", maxCost=" + maxCost
				+ ", travelTime=" + travelTime + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((destination == null) ? 0 : destination.hashCode());
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result + ((guest == null) ? 0 : guest.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((maxCost == null) ? 0 : maxCost.hashCode());
		result = prime * result + ((minCost == null) ? 0 : minCost.hashCode());
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		result = prime * result + ((travelTime == null) ? 0 : travelTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UberLogModel other = (UberLogModel) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		if (guest == null) {
			if (other.guest != null)
				return false;
		} else if (!guest.equals(other.guest))
			return false;
		if (id != other.id)
			return false;
		if (maxCost == null) {
			if (other.maxCost != null)
				return false;
		} else if (!maxCost.equals(other.maxCost))
			return false;
		if (minCost == null) {
			if (other.minCost != null)
				return false;
		} else if (!minCost.equals(other.minCost))
			return false;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		if (travelTime == null) {
			if (other.travelTime != null)
				return false;
		} else if (!travelTime.equals(other.travelTime))
			return false;
		return true;
	}

}
