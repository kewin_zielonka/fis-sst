package org.uber.backend.models;

public class Product {

	private int capacity;
	private String productId;
	private Double costPerMin;
	private String unit;
	private Double minimum;
	private Double costPerDistance;
	private Double base;
	private String currency;
	private String imageUrl;
	private boolean cashEnabled;
	private boolean shared;
	private String shortDescription;
	private String displayName;
	private String productGroup;
	private String description;

	public Product(int capacity, String productId, Double costPerMin, String unit, Double minimum,
			Double costPerDistance, Double base, String currency, String imageUrl, boolean cashEnabled, boolean shared,
			String shortDescription, String displayName, String productGroup, String description) {
		super();
		this.capacity = capacity;
		this.productId = productId;
		this.costPerMin = costPerMin;
		this.unit = unit;
		this.minimum = minimum;
		this.costPerDistance = costPerDistance;
		this.base = base;
		this.currency = currency;
		this.imageUrl = imageUrl;
		this.cashEnabled = cashEnabled;
		this.shared = shared;
		this.shortDescription = shortDescription;
		this.displayName = displayName;
		this.productGroup = productGroup;
		this.description = description;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public Double getCostPerMin() {
		return costPerMin;
	}

	public void setCostPerMin(Double costPerMin) {
		this.costPerMin = costPerMin;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getMinimum() {
		return minimum;
	}

	public void setMinimum(Double minimum) {
		this.minimum = minimum;
	}

	public Double getCostPerDistance() {
		return costPerDistance;
	}

	public void setCostPerDistance(Double costPerDistance) {
		this.costPerDistance = costPerDistance;
	}

	public Double getBase() {
		return base;
	}

	public void setBase(Double base) {
		this.base = base;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public boolean isCashEnabled() {
		return cashEnabled;
	}

	public void setCashEnabled(boolean cashEnabled) {
		this.cashEnabled = cashEnabled;
	}

	public boolean isShared() {
		return shared;
	}

	public void setShared(boolean shared) {
		this.shared = shared;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Product [capacity=" + capacity + ", productId=" + productId + ", costPerMin=" + costPerMin + ", unit="
				+ unit + ", minimum=" + minimum + ", costPerDistance=" + costPerDistance + ", base=" + base
				+ ", currency=" + currency + ", imageUrl=" + imageUrl + ", cashEnabled=" + cashEnabled + ", shared="
				+ shared + ", shortDescription=" + shortDescription + ", displayName=" + displayName + ", productGroup="
				+ productGroup + ", description=" + description + "]";
	}

}
