package org.uber.backend.models;

import java.math.BigDecimal;

public class UberListModel {

	private String name;
	private BigDecimal minCost;
	private BigDecimal maxCost;
	private String currency;
	private Integer estimatedTime;
	private String productId;
	private float lat1, lat2, lng1, lng2;

	public UberListModel(String name, BigDecimal minCost, BigDecimal maxCost, String currency, Integer estimatedTime,
			String productId, float lat1, float lng1, float lat2, float lng2) {
		super();
		this.name = name;
		this.minCost = minCost;
		this.maxCost = maxCost;
		this.currency = currency;
		this.estimatedTime = estimatedTime;
		this.productId = productId;
		this.lat1 = lat1;
		this.lat2 = lat2;
		this.lng1 = lng1;
		this.lng2 = lng2;
	}

	public UberListModel(String name, BigDecimal minCost, BigDecimal maxCost, String currency, Integer estimatedTime,
			String productId) {
		super();
		this.name = name;
		this.minCost = minCost;
		this.maxCost = maxCost;
		this.currency = currency;
		this.estimatedTime = estimatedTime;
		this.productId = productId;

	}

	public float getLat1() {
		return lat1;
	}

	public void setLat1(float lat1) {
		this.lat1 = lat1;
	}

	public float getLat2() {
		return lat2;
	}

	public void setLat2(float lat2) {
		this.lat2 = lat2;
	}

	public float getLng1() {
		return lng1;
	}

	public void setLng1(float lng1) {
		this.lng1 = lng1;
	}

	public float getLng2() {
		return lng2;
	}

	public void setLng2(float lng2) {
		this.lng2 = lng2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMinCost() {
		return minCost + " " + currency;
	}

	public void setMinCost(BigDecimal minCost) {
		this.minCost = minCost;
	}

	public String getMaxCost() {
		return maxCost + " " + currency;
	}

	public void setMaxCost(BigDecimal maxCost) {
		this.maxCost = maxCost;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Integer getEstimatedTime() {
		return estimatedTime / 60;
		// minuty
	}

	public void setEstimatedTime(Integer estimatedTime) {
		this.estimatedTime = estimatedTime;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "UberListModel [name=" + name + ", minCost=" + minCost + ", maxCost=" + maxCost + ", currency="
				+ currency + ", estimatedTime=" + estimatedTime + ", productId=" + productId + "]";
	}

}
