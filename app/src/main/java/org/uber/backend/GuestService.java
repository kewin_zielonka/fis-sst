package org.uber.backend;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;

import org.uber.backend.dao.DaoGuest;
import org.uber.backend.dao.implement.DaoGuestImpl;
import org.uber.backend.models.Guest;

@ApplicationScoped
public class GuestService implements UberService<Guest> {
	private DaoGuest daoGuest = new DaoGuestImpl();

	public List<Guest> getGuestList() {
		return daoGuest.getAllGuests();
	}

	public void saveGuest(Guest guest) {
		daoGuest.saveGuest(guest);
	}

	public Guest getGuestById(Long id) {
		return daoGuest.getGuest(id);
	}

	public void updateGuest(Guest guest) {
		daoGuest.updateGuest(guest);
	}

	@Override
	public List<Guest> getLimitedList(int first, int max) {
		return daoGuest.getLimitedGuestsList(first, max);
	}

	@Override
	public <T> void delete(T t) {
		daoGuest.deleteGuest((Guest) t);
	}

	@Override
	public int getNumberOfRows() {
		return daoGuest.countGuests();
	}

	@Override
	public Set<Guest> search(String phrase) {
		Set<Guest> result = new HashSet<>();
		result.addAll(daoGuest.searchGuestByName(phrase));
		result.addAll(daoGuest.searchGuestByLastName(phrase));
		return result;
	}
}
