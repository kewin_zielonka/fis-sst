package org.uber.backend;

import java.util.List;
import java.util.Set;

public interface UberService<T> {

	public int getNumberOfRows();

	public List<T> getLimitedList(int first, int max);
	
	public Set<T> search(String phrase);

	@SuppressWarnings("hiding")
	public <T> void delete(T t);
}
