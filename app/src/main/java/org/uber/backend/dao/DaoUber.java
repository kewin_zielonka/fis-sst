package org.uber.backend.dao;

import java.io.IOException;
import java.util.List;

import javax.enterprise.context.SessionScoped;

import org.apache.http.client.ClientProtocolException;
import org.uber.backend.models.Guest;
import org.uber.backend.models.LocationEntity;
import org.uber.backend.models.UberListModel;
import org.uber.backend.models.UberLogModel;

import com.uber.sdk.rides.client.model.PriceEstimatesResponse;
import com.uber.sdk.rides.client.model.ProductsResponse;

@SessionScoped
public interface DaoUber {

	public ProductsResponse findUber(float lat, float lng) throws IOException;

	public List<LocationEntity> getLocations();

	public long estimateTime(float lat, float lng, String productId) throws IOException;

	public PriceEstimatesResponse estimatePrice(float lat1, float lng1, float lat2, float lng2) throws IOException;

	public List<UberListModel> getUberList(float lat1, float lng1, float lat2, float lng2) throws IOException;

	public List<UberListModel> getUberList(String from, String to, String guest) throws IOException;

	public List<UberLogModel> getLogList() throws IOException;

	public LocationEntity getLocation(Long id);

	public int getLocation(String name);

	public void deleteLocation(LocationEntity locationEntity);

	public void saveLocation(LocationEntity locationEntity);

	public void updateLocation(LocationEntity locationEntity);
	
	public String getRedirectUrl(float lat1, float lng1, float lat2, float lng2, String productId)
			throws ClientProtocolException, IOException;

	public String requestUber(float lat1, float lng1, float lat2, float lng2, String productId, String code)
			throws ClientProtocolException, IOException;

	public List<UberLogModel> getLimitedLogList(int first, int max);

	public List<LocationEntity> getLimitedLocations(int first, int max);

	public int countLog();

	public int countLocations();
	
	public List<LocationEntity> searchLocByName(String name);
	
	public List<LocationEntity> searchLocByAddress(String address);
	
	public List<UberLogModel> searchLogByGuest(String guest);
	
	public List<UberLogModel> searchLogByLocFrom(String locFrom);
	
	public List<UberLogModel> searchLogByLocDest(String locDest);

}
