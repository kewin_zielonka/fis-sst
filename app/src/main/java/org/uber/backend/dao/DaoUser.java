package org.uber.backend.dao;

import java.util.List;

import javax.enterprise.context.SessionScoped;

import org.uber.backend.models.Guest;
import org.uber.backend.models.User;

@SessionScoped
public interface DaoUser {

	public List<User> getAllUsers();

	public User getUser(String login);

	public void updateUser(User user);

	public void deleteUser(User user);

	public boolean validateUser(String login, String password);

	public void saveUser(User user);

	public int countGuests();

	public List<User> getLimitedUsersList(int first, int max);
	
	public List<User> searchUserByLogin(String login);

}
