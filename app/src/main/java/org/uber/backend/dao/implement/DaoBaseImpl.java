package org.uber.backend.dao.implement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DaoBaseImpl {

	private EntityManagerFactory entityManagerFactory;
	private EntityManager entityManager;

	public DaoBaseImpl() {
		entityManagerFactory = Persistence.createEntityManagerFactory("persistence");
		entityManager = entityManagerFactory.createEntityManager();
	}

	protected EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	protected void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	protected EntityManager getEntityManager() {
		return entityManager;
	}

	protected void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
