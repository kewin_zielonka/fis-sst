package org.uber.backend.dao;

import java.util.List;

import javax.enterprise.context.SessionScoped;

import org.uber.backend.models.Guest;

@SessionScoped
public interface DaoGuest {

	public List<Guest> getAllGuests();

	public Guest getGuest(Long id);

	public void updateGuest(Guest guest);

	public void deleteGuest(Guest guest);

	public void saveGuest(Guest guest);

	public int countGuests();

	public List<Guest> getLimitedGuestsList(int first, int max);
	
	public List<Guest> searchGuestByName(String name);
	
	public List<Guest> searchGuestByLastName(String lastName);
}
