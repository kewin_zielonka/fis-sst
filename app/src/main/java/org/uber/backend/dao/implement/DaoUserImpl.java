package org.uber.backend.dao.implement;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.uber.backend.dao.DaoUser;
import org.uber.backend.models.User;

public class DaoUserImpl extends DaoBaseImpl implements DaoUser {

	@Override
	public List<User> getAllUsers() {
		@SuppressWarnings("unchecked")
		List<User> list = getEntityManager().createQuery("SELECT e FROM User e").getResultList();
		return list;
	}

	@Override
	public User getUser(String login) {

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<User> cq = cb.createQuery(User.class);
		Root<User> usr = cq.from(User.class);
		cq.select(usr);
		TypedQuery<User> q = getEntityManager().createQuery(cq);

		List<User> allUsers = q.getResultList();
		for (User x : allUsers) {
			if (x.getLogin() != null)
				if (x.getLogin().toUpperCase().equals(login.toUpperCase()))
					return x;
		}
		return null;
	}

	@Override
	public void updateUser(User user) {
		EntityManager entityManager = getEntityManager();
		entityManager.getTransaction().begin();
		entityManager.merge(user);
		entityManager.getTransaction().commit();

	}

	@Override
	public void deleteUser(User user) {
		EntityManager entityManager = getEntityManager();
		entityManager.getTransaction().begin();
		entityManager.remove(user);
		entityManager.getTransaction().commit();
	}

	@Override
	public void saveUser(User user) {

		getEntityManager().getTransaction().begin();
		getEntityManager().persist(user);
		getEntityManager().getTransaction().commit();
	}

	@Override
	public boolean validateUser(String login, String password) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int countGuests() {
		return Integer
				.parseInt(getEntityManager().createQuery("Select count(*) from User").getSingleResult().toString());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getLimitedUsersList(int first, int max) {
		return getEntityManager().createQuery("from User").setFirstResult(first).setMaxResults(max).getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> searchUserByLogin(String login) {
		return getEntityManager().createQuery("from User where LOWER(login) like LOWER(:login)").setParameter("login", "%"+login+"%").getResultList();
	}

}
