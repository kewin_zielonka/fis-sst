package org.uber.backend.dao.implement;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.uber.backend.dao.DaoUber;
import org.uber.backend.models.LocationEntity;
import org.uber.backend.models.UberListModel;
import org.uber.backend.models.UberLogModel;
import org.uber.views.LoginAction;

import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.auth.oauth2.Credential;
import com.uber.sdk.core.auth.Scope;
import com.uber.sdk.rides.auth.AuthException;
import com.uber.sdk.rides.auth.OAuth2Credentials;
import com.uber.sdk.rides.client.CredentialsSession;
import com.uber.sdk.rides.client.ServerTokenSession;
import com.uber.sdk.rides.client.SessionConfiguration;
import com.uber.sdk.rides.client.SessionConfiguration.Environment;
import com.uber.sdk.rides.client.UberRidesApi;
import com.uber.sdk.rides.client.model.PriceEstimate;
import com.uber.sdk.rides.client.model.PriceEstimatesResponse;
import com.uber.sdk.rides.client.model.ProductsResponse;
import com.uber.sdk.rides.client.model.Ride;
import com.uber.sdk.rides.client.model.RideRequestParameters;
import com.uber.sdk.rides.client.model.TimeEstimatesResponse;
import com.uber.sdk.rides.client.services.RidesService;

import retrofit2.Response;

public class DaoUberImpl extends DaoBaseImpl implements DaoUber {

	private final static Logger logger = Logger.getLogger(LoginAction.class);

	@Override
	public ProductsResponse findUber(float lat, float lng) throws IOException {

		SessionConfiguration config = new SessionConfiguration.Builder().setClientId("WivjIfrgQS_Xvfn1Cmz91eQWSys21wLK")
				.setEnvironment(Environment.SANDBOX).setServerToken("z8-pbbZj1l-9gk602WEQmsaRYkiBbJlvv7i3g8-E").build();

		ServerTokenSession session = new ServerTokenSession(config);

		UberRidesApi uberRidesApi = UberRidesApi.with(session).build();

		RidesService service = uberRidesApi.createService();

		ProductsResponse productResponse = null;

		Response<ProductsResponse> response;
		// Response<TimeEstimatesResponse> responseTime =
		// service.getPickupTimeEstimate(37.79f, -122.39f, productId).execute()
		try {
			response = service.getProducts(lat, lng).execute();
			productResponse = response.body();
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e);
			throw new IOException(e);
		}
		return productResponse;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LocationEntity> getLocations() {
		List<LocationEntity> list = new LinkedList<LocationEntity>();
		list = getEntityManager().createQuery("SELECT e FROM LocationEntity e").getResultList();
		return list;
	}

	@Override
	public long estimateTime(float lat, float lng, String productId) throws IOException {

		SessionConfiguration config = new SessionConfiguration.Builder().setClientId("WivjIfrgQS_Xvfn1Cmz91eQWSys21wLK")
				.setEnvironment(Environment.SANDBOX).setServerToken("z8-pbbZj1l-9gk602WEQmsaRYkiBbJlvv7i3g8-E").build();

		ServerTokenSession session = new ServerTokenSession(config);

		UberRidesApi uberRidesApi = UberRidesApi.with(session).build();

		RidesService service = uberRidesApi.createService();
		Response<TimeEstimatesResponse> pickupTimeResponse = service.getPickupTimeEstimate(lat, lng, productId)
				.execute();
		TimeEstimatesResponse pickupTime = pickupTimeResponse.body();
		pickupTime.getTimes().get(0);

		// RideRequestParameters rideRequestParameters =

		// service.estimateRide(rideRequestParameters);

		return pickupTime.getTimes().get(0).getEstimate();
	}

	@Override
	public PriceEstimatesResponse estimatePrice(float lat1, float lng1, float lat2, float lng2) throws IOException {

		SessionConfiguration config = new SessionConfiguration.Builder().setClientId("WivjIfrgQS_Xvfn1Cmz91eQWSys21wLK")
				.setEnvironment(Environment.SANDBOX).setServerToken("z8-pbbZj1l-9gk602WEQmsaRYkiBbJlvv7i3g8-E").build();

		ServerTokenSession session = new ServerTokenSession(config);

		UberRidesApi uberRidesApi = UberRidesApi.with(session).build();

		RidesService service = uberRidesApi.createService();
		Response<PriceEstimatesResponse> estimatedPriceResponse;
		estimatedPriceResponse = service.getPriceEstimates(lat1, lng1, lat2, lng2).execute();
		PriceEstimatesResponse estimatePrice = estimatedPriceResponse.body();

		return estimatePrice;
	}

	@Override
	public List<UberListModel> getUberList(float lat1, float lng1, float lat2, float lng2) throws IOException {

		List<UberListModel> uberList = new LinkedList<UberListModel>();
		PriceEstimatesResponse estimatePrice = estimatePrice(lat1, lng1, lat2, lng2);

		for (PriceEstimate x : estimatePrice.getPrices()) {
			uberList.add(new UberListModel(x.getDisplayName(), x.getLowEstimate(), x.getHighEstimate(),
					x.getCurrencyCode(), x.getDuration(), x.getProductId(), lat1, lng1, lat2, lng2));
		}
		return uberList;
	}

	@Override
	public List<UberListModel> getUberList(String from, String to, String guest) throws IOException {

		logger.debug("Getting Uber list to " + guest + " from " + from + " to " + to);

		List<UberListModel> uberList = new LinkedList<UberListModel>();

		float lat1 = 0, lat2 = 0, lng1 = 0, lng2 = 0;

		for (LocationEntity x : getLocations()) {
			if (x.getName().equals(from)) {
				lat1 = (float) x.getLatitude();
				lng1 = (float) x.getLongitude();
			}
			if (x.getName().equals(to)) {
				lat2 = (float) x.getLatitude();
				lng2 = (float) x.getLongitude();
			}
		}

		PriceEstimatesResponse estimatePrice = estimatePrice(lat1, lng1, lat2, lng2);
		if (estimatePrice != null) {
			for (PriceEstimate x : estimatePrice.getPrices()) {
				uberList.add(new UberListModel(x.getDisplayName(), x.getLowEstimate(), x.getHighEstimate(),
						x.getCurrencyCode(), x.getDuration(), x.getProductId(), lat1, lng1, lat2, lng2));
			}

			logUberSearch(uberList, guest, from, to);
		}
		return uberList;
	}

	@Override
	public List<UberLogModel> getLogList() throws IOException {
		logger.debug("Getting search log list...");
		@SuppressWarnings("unchecked")
		List<UberLogModel> list = getEntityManager().createQuery("SELECT e FROM UberLogModel e order by 1 desc")
				.getResultList();
		return list;
	}

	public void logUberSearch(List<UberListModel> list, String guest, String from, String to) throws IOException {

		if (!list.isEmpty()) {
			logger.debug("Loging Uber search to results: \n" + list.toString());

			java.util.Date date = new Date();
			Timestamp timestamp = new Timestamp(date.getTime());
			EntityManager entityManager = getEntityManager();

			for (UberListModel x : list) {
				UberLogModel uberLogModel = new UberLogModel(guest, timestamp, from, to, x.getName(), x.getMinCost(),
						x.getMaxCost(), x.getEstimatedTime() + " min");
				entityManager.getTransaction().begin();
				entityManager.persist(uberLogModel);
				entityManager.getTransaction().commit();
			}
		}
	}

	@Override
	public LocationEntity getLocation(Long id) {
		return (LocationEntity) getEntityManager().createQuery("from LocationEntity where id=:ID")
				.setParameter("ID", id).getSingleResult();

	}

	@Override
	public int getLocation(String name) {
		return getEntityManager().createQuery("from LocationEntity where name=:NAME").setParameter("NAME", name)
				.getResultList().size();

	}

	@Override
	public void deleteLocation(LocationEntity locationEntity) {

		EntityManager entityManager = getEntityManager();
		entityManager.getTransaction().begin();
		entityManager.remove(locationEntity);
		entityManager.getTransaction().commit();
	}

	@Override
	public void saveLocation(LocationEntity locationEntity) {
		getEntityManager().getTransaction().begin();
		getEntityManager().persist(locationEntity);
		getEntityManager().getTransaction().commit();

	}

	@Override
	public void updateLocation(LocationEntity locationEntity) {
		EntityManager entityManager = getEntityManager();
		entityManager.getTransaction().begin();
		entityManager.merge(locationEntity);
		entityManager.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UberLogModel> getLimitedLogList(int first, int max) {
		return getEntityManager().createQuery("SELECT e FROM UberLogModel e order by 1 desc").setFirstResult(first)
				.setMaxResults(max).getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LocationEntity> getLimitedLocations(int first, int max) {
		return getEntityManager().createQuery("from LocationEntity").setFirstResult(first).setMaxResults(max)
				.getResultList();
	}

	@Override
	public int countLog() {
		return Integer.parseInt(
				getEntityManager().createQuery("Select count(*) from UberLogModel").getSingleResult().toString());
	}

	@Override
	public int countLocations() {
		return Integer.parseInt(
				getEntityManager().createQuery("Select count(*) from LocationEntity").getSingleResult().toString());

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LocationEntity> searchLocByName(String name) {
		return getEntityManager().createQuery("from LocationEntity where LOWER(name) like LOWER(:name)")
				.setParameter("name", "%" + name + "%").getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LocationEntity> searchLocByAddress(String address) {
		return getEntityManager().createQuery("from LocationEntity where LOWER(address) like LOWER(:address)")
				.setParameter("address", "%" + address + "%").getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UberLogModel> searchLogByGuest(String guest) {
		return getEntityManager().createQuery("from UberLogModel where LOWER(guest) like LOWER(:guest)")
				.setParameter("guest", "%" + guest + "%").getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UberLogModel> searchLogByLocFrom(String locFrom) {
		return getEntityManager().createQuery("from UberLogModel where LOWER(from_place) like LOWER(:locFrom)")
				.setParameter("locFrom", "%" + locFrom + "%").getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UberLogModel> searchLogByLocDest(String locDest) {
		return getEntityManager().createQuery("from UberLogModel where LOWER(destination) like LOWER(:destination)")
				.setParameter("destination", "%" + locDest + "%").getResultList();
	}

	@Override
	public String requestUber(float lat1, float lng1, float lat2, float lng2, String productId, String code)
			throws ClientProtocolException, IOException {
		String clientId = "WivjIfrgQS_Xvfn1Cmz91eQWSys21wLK";
		String clientSecret = "39zizcJi10Jkb-wRDIZGTKv9ZV7gKyUwp0CFC5FA";
		String serverToken = "z8-pbbZj1l-9gk602WEQmsaRYkiBbJlvv7i3g8-E";
		String rideStatus = null;
		String rideId = null;

		SessionConfiguration config = new SessionConfiguration.Builder().setClientId(clientId)
				.setClientSecret(clientSecret).setServerToken(serverToken).setEnvironment(Environment.SANDBOX)
				.setScopes(Arrays.asList(Scope.PROFILE, Scope.REQUEST)).setRedirectUri("https://16-233.sst.pl/Uber/")
				// .setRedirectUri("https://login.uber.com/oauth/authorize")
				.build();

		try {
			OAuth2Credentials credentials = new OAuth2Credentials.Builder().setSessionConfiguration(config).build();

			credentials.getAuthorizationUrl();

			Credential credential = credentials.authenticate(code, clientId);
			CredentialsSession session = new CredentialsSession(config, credential);
			RidesService service = UberRidesApi.with(session).build().createService();

			RideRequestParameters rideRequestParameters = new RideRequestParameters.Builder()
					.setPickupCoordinates(lat1, lng1).setProductId(productId).setDropoffCoordinates(lat2, lng2).build();
			Response<Ride> rideResponse = service.requestRide(rideRequestParameters).execute();
			Ride ride = rideResponse.body();

			if (ride != null) {
				rideId = ride.getRideId();
				Ride rideDetails = service.getRideDetails(rideId).execute().body();
				rideStatus = rideDetails.getStatus().name();
				logger.info("Ride status: " + rideStatus);
			}
			else {
				rideStatus = "Error, "+rideResponse.errorBody().string();
			}
		} catch (AuthException e) {
			throw new IOException(e);
		}

		return rideStatus;
	}

	@Override
	public String getRedirectUrl(float lat1, float lng1, float lat2, float lng2, String productId)
			throws ClientProtocolException, IOException {

		String clientId = "WivjIfrgQS_Xvfn1Cmz91eQWSys21wLK";
		String clientSecret = "39zizcJi10Jkb-wRDIZGTKv9ZV7gKyUwp0CFC5FA";
		String serverToken = "z8-pbbZj1l-9gk602WEQmsaRYkiBbJlvv7i3g8-E";
		String authorizationUrl = null;

		SessionConfiguration config = new SessionConfiguration.Builder().setClientId(clientId)
				.setClientSecret(clientSecret).setServerToken(serverToken).setEnvironment(Environment.SANDBOX)
				.setScopes(Arrays.asList(Scope.PROFILE, Scope.REQUEST)).setRedirectUri("https://16-233.sst.pl/Uber/")
				// .setRedirectUri("https://login.uber.com/oauth/authorize")
				.build();

		try {
			OAuth2Credentials credentials = new OAuth2Credentials.Builder().setSessionConfiguration(config).build();

			authorizationUrl = credentials.getAuthorizationUrl();

			logger.info("Authorization URL: " + authorizationUrl);
		} catch (AuthException e) {
			throw new IOException(e);
		}

		return authorizationUrl;
	}

}
