package org.uber.backend.dao.implement;

import java.util.List;

import javax.persistence.EntityManager;

import org.uber.backend.dao.DaoGuest;
import org.uber.backend.models.Guest;

public class DaoGuestImpl extends DaoBaseImpl implements DaoGuest {

	@SuppressWarnings("unchecked")
	@Override
	public List<Guest> getAllGuests() {
		List<Guest> list = getEntityManager().createQuery("SELECT e FROM Guest e").getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Guest> getLimitedGuestsList(int first, int max) {
		return getEntityManager().createQuery("from Guest").setFirstResult(first).setMaxResults(max).getResultList();
	}

	@Override
	public Guest getGuest(Long id) {
		return (Guest) getEntityManager().createQuery("from Guest where id=:ID").setParameter("ID", id)
				.getSingleResult();
	}

	@Override
	public void updateGuest(Guest guest) {
		EntityManager entityManager = getEntityManager();
		entityManager.getTransaction().begin();
		entityManager.merge(guest);
		entityManager.getTransaction().commit();

	}

	@Override
	public void deleteGuest(Guest guest) {
		EntityManager entityManager = getEntityManager();
		entityManager.getTransaction().begin();
		entityManager.remove(guest);
		entityManager.getTransaction().commit();
	}

	@Override
	public void saveGuest(Guest guest) {

		getEntityManager().getTransaction().begin();
		getEntityManager().persist(guest);
		getEntityManager().getTransaction().commit();
	}

	@Override
	public int countGuests() {
		return Integer
				.parseInt(getEntityManager().createQuery("Select count(*) from Guest").getSingleResult().toString());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Guest> searchGuestByName(String name) {
		return getEntityManager().createQuery("from Guest where LOWER(name) like LOWER(:name)").setParameter("name", "%"+name+"%").getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Guest> searchGuestByLastName(String lastName) {
		return getEntityManager().createQuery("from Guest where LOWER(last_name) like LOWER(:lastName)").setParameter("lastName", "%"+lastName+"%").getResultList();
	}

}
