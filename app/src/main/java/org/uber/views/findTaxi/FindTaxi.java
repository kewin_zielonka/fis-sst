package org.uber.views.findTaxi;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.uber.backend.GuestService;
import org.uber.backend.dao.DaoGuest;
import org.uber.backend.dao.DaoUber;
import org.uber.backend.dao.implement.DaoGuestImpl;
import org.uber.backend.dao.implement.DaoUberImpl;
import org.uber.backend.models.Guest;
import org.uber.backend.models.LocationEntity;
import org.uber.backend.models.UberListModel;
import org.uber.views.IDMInitializer;
import org.uber.views.TestUI;
import org.uber.views.guest.AddNewGuestView;
import org.vaadin.viritin.FilterableListContainer;

import com.vaadin.annotations.Theme;
import com.vaadin.cdi.CDIUI;
import com.vaadin.data.Property;
import com.vaadin.data.validator.NullValidator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Responsive;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Table.TableDragMode;
import com.vaadin.ui.themes.ValoTheme;

@CDIUI(value = "FindTaxi")
@Theme("valo")
@SuppressWarnings("serial")
public class FindTaxi extends VerticalLayout implements View {

	// @Inject
	private DaoUber daoUber;
	// @Inject
	private DaoGuest daoGuest = new DaoGuestImpl();
	private List<UberListModel> uberList;
	private Table table = new Table();
	private final static Logger logger = Logger.getLogger(FindTaxi.class);

	public FindTaxi() {
		daoUber = TestUI.getHistoryService().getUberDao();
		setSizeFull();
		VerticalLayout vertical = new VerticalLayout();
		addComponent(vertical);
		vertical.addComponent(buildHeader());
		FormLayout form = new FormLayout();
		form.setMargin(true);

		vertical.addComponent(form);

		ComboBox cb1 = new ComboBox("Kto:");
		cb1.setFilteringMode(FilteringMode.CONTAINS);
		for (Guest x : daoGuest.getAllGuests())
			cb1.addItem(x.getName() + " " + x.getLastName());
		cb1.setIcon(FontAwesome.USER);
		cb1.setRequired(true);
		cb1.addValidator(new NullValidator("Must be given", false));
		form.addComponent(cb1);

		ComboBox cb2 = new ComboBox("Skąd:");
		cb2.setFilteringMode(FilteringMode.CONTAINS);
		for (LocationEntity x : daoUber.getLocations())
			cb2.addItem(x.getName());
		cb2.setIcon(FontAwesome.HOME);
		cb2.setRequired(true);
		cb2.addValidator(new NullValidator("Must be given", false));
		form.addComponent(cb2);

		ComboBox cb3 = new ComboBox("Dokąd:");
		cb3.setFilteringMode(FilteringMode.CONTAINS);
		for (LocationEntity x : daoUber.getLocations())
			cb3.addItem(x.getName());
		cb3.setIcon(FontAwesome.QUESTION_CIRCLE);
		cb3.setRequired(true);
		cb3.addValidator(new NullValidator("Must be given", false));
		form.addComponent(cb3);
		vertical.setMargin(true);

		Button save = new Button("Szukaj", e -> {
			Object value1 = cb1.getValue();
			Object value2 = cb2.getValue();
			Object value3 = cb3.getValue();

			if (value1 != null && (value2 != value3) && value2 != null && value3 != null) {
				try {
					vertical.removeComponent(table);
					uberList = daoUber.getUberList((String) value2, (String) value3, (String) value1);
					if (uberList.size() > 0) {
						table = buildTable();

						loadDataIntoTable(uberList, (String) value1, (String) value2, (String) value3);

						vertical.addComponent(table);

					} else {
						Notification.show("To połączenie nie jest realizowane");
					}
				} catch (IOException e1) {
					e1.printStackTrace();
					logger.error(e1);
				}
			} else
				Notification.show("Popraw dane i spróbuj jeszcze raz");

		});
		save.setId("serch");
		form.addComponent(save);

		String code = (String) VaadinService.getCurrentRequest().getWrappedSession().getAttribute("code");
		String status = null;

		if (code != null){
			status = requestUber(code);
			if (status != null) Notification.show("Status zamówienia UBER: " + status);
			else Notification.show("Uber nie został zamówiony, wystąpił problem...");
		}
		
		

		Responsive.makeResponsive(this);

	}

	private void loadDataIntoTable(List<UberListModel> uberList2, String value1, String value2, String value3) {
		for (UberListModel location : uberList2) {
			Button detailsField = new Button("Zamów", FontAwesome.AUTOMOBILE);
			detailsField.setData(location.getProductId());
			detailsField.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					String redirectUrl = saveInSession(location.getProductId(), location.getLat1(), location.getLng1(),
							location.getLat2(), location.getLng2());
					if (redirectUrl != null)
						getUI().getPage().setLocation(redirectUrl);
				}
			});
			table.addItem(new Object[] { location.getName(), location.getMinCost(), location.getMaxCost(),
					location.getEstimatedTime().toString(), detailsField }, null);

		}
	}

	private Component buildHeader() {
		HorizontalLayout header = new HorizontalLayout();
		header.addStyleName("viewheader");
		header.setSpacing(true);
		Responsive.makeResponsive(header);

		Label titleLabel = new Label("Znajdź taxi");
		titleLabel.setSizeUndefined();
		titleLabel.addStyleName(ValoTheme.LABEL_H1);
		titleLabel.addStyleName(ValoTheme.LABEL_NO_MARGIN);
		header.addComponents(titleLabel);

		return header;
	}

	private Table buildTable() {
		Table table = new Table();
		table.addStyleName("components-inside");
		table.addContainerProperty("Nazwa uslugi", String.class, null);
		table.addContainerProperty("Koszt min.", String.class, null);
		table.addContainerProperty("Koszt max.", String.class, null);
		table.addContainerProperty("Czas dojazdu [min]", String.class, null);

		table.addContainerProperty("", Button.class, null);

		table.addItemClickListener(e -> {
			Long selectedId = Long.parseLong(e.getItemId().toString());
		});
		table.setImmediate(true);
		table.setPageLength(table.size());
		return table;
	}

	private String saveInSession(String productId, float lat1, float lng1, float lat2, float lng2) {

		VaadinService.getCurrentRequest().getWrappedSession().setAttribute("productId", productId);
		VaadinService.getCurrentRequest().getWrappedSession().setAttribute("lat1", lat1);
		VaadinService.getCurrentRequest().getWrappedSession().setAttribute("lat2", lat2);
		VaadinService.getCurrentRequest().getWrappedSession().setAttribute("lng1", lng1);
		VaadinService.getCurrentRequest().getWrappedSession().setAttribute("lng2", lng2);

		try {
			return daoUber.getRedirectUrl(lat1, lng1, lat2, lng2, productId);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	private String requestUber(String code) {
		Object productIdOb = VaadinService.getCurrentRequest().getWrappedSession().getAttribute("productId");
		Object lat1Ob = VaadinService.getCurrentRequest().getWrappedSession().getAttribute("lat1");
		Object lat2Ob = VaadinService.getCurrentRequest().getWrappedSession().getAttribute("lat2");
		Object lng1Ob = VaadinService.getCurrentRequest().getWrappedSession().getAttribute("lng1");
		Object lng2Ob = VaadinService.getCurrentRequest().getWrappedSession().getAttribute("lng2");

		String productId = productIdOb != null ? productIdOb.toString() : null;
		float lat1 = lat1Ob != null ? (float) lat1Ob : 0;
		float lng1 = lng1Ob != null ? (float) lng1Ob : 0;
		float lat2 = lat2Ob != null ? (float) lat2Ob : 0;
		float lng2 = lng2Ob != null ? (float) lng2Ob : 0;

		String status = null;

		if (productId != null && lat1 != 0 && lat2 != 0 && lng1 != 0 && lng2 != 0) {
			try {
				status = daoUber.requestUber(lat1, lng1, lat2, lng2, productId, code);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				VaadinService.getCurrentRequest().getWrappedSession().removeAttribute("code");
				VaadinService.getCurrentRequest().getWrappedSession().removeAttribute("lat1");
				VaadinService.getCurrentRequest().getWrappedSession().removeAttribute("lng1");
				VaadinService.getCurrentRequest().getWrappedSession().removeAttribute("lat2");
				VaadinService.getCurrentRequest().getWrappedSession().removeAttribute("lng2");
			}
		}

		return status;
	}

	private class TempTransactionsContainer extends FilterableListContainer<UberListModel> {

		public TempTransactionsContainer(final Collection<UberListModel> collection) {
			super(collection);
		}
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub

	}

}
