package org.uber.views;

import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.picketlink.Identity;
import org.picketlink.Identity.AuthenticationResult;
import org.picketlink.idm.model.Account;

public @RequestScoped @Named class LoginAction {

	@Inject
	Identity identity;
	
	private final static Logger logger = Logger.getLogger(LoginAction.class);

	public AuthenticationResult login() {
		
		if (identity.isLoggedIn()) {
			logger.info("Juz był zalogowany");
			return AuthenticationResult.SUCCESS;
		}
		AuthenticationResult result = identity.login();

		if (AuthenticationResult.FAILED.equals(result)) {
			logger.info("Nie zalogowało");
		}
		if (AuthenticationResult.SUCCESS.equals(result)) {
			logger.info("Zalogowało mnieeeee");
		}
		return result;
	}
}