package org.uber.views.localization;

import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.uber.backend.LocationService;
import org.uber.backend.models.LocationEntity;
import org.uber.views.TestUI;

import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")

public final class AddNewLocation extends VerticalLayout{
	private LocationView localizationView;
	private TextField localizationName;
	private LocationEntity location;
	private TextField place;
	private TextField street;

	private final static Logger logger = Logger.getLogger(AddNewLocation.class);

	public AddNewLocation(LocationView localizationView) {
	 setSizeFull();
	this.localizationView = localizationView;
	        addStyleName("transactions");

	        addComponent( buildUI());
		Responsive.makeResponsive(this);

	}
	public AddNewLocation() {
	    addComponent( buildUI());	}
	
	private Component buildUI() {
		VerticalLayout panel = new VerticalLayout();
        panel.addStyleName("viewheader");
        panel.setSpacing(true);
        Responsive.makeResponsive(panel);
        panel.setDefaultComponentAlignment(Alignment.BOTTOM_CENTER);
        
        Label title = new Label("Dodaj lokalizacje");
        title.setSizeUndefined();        
        panel.addComponent(title);
        
       localizationName = new TextField("Nazwa lokalizacji");
       localizationName.setId("name");
        panel.addComponent(localizationName);
        localizationName.addValidator(new StringLengthValidator(
        	    "Nazwa lokazlizacji musi składać się przyjanmniej z 2 liter",
        	    2, 25, false));
        
         place= new TextField("Miejscowość");
         place.setId("city");
        panel.addComponent(place);
        street = new TextField("Ulica");
        street.setId("street");
        panel.addComponent(street);
       

        
        
        localizationName.setValidationVisible(false);

        Button btnSave = new Button("Zapisz",FontAwesome.SAVE);
        btnSave.setId("save");
        panel.addComponent(btnSave);
        btnSave.addClickListener(new ClickListener() {
        	
			@Override
			public void buttonClick(ClickEvent event) {
			        try {
			        	localizationName.validate();
			        	if(location==null)
			        	{
			        		String address = place.getValue()+ ", "+street.getValue();
						TestUI.getLocalizationService().saveLocations(new LocationEntity(localizationName.getValue(),address));
			        	}
			        	else
			        	{
			        		location.setName(localizationName.getValue());
			        		String address = place.getValue()+ ", "+street.getValue();
			        		location.setAddress(address);
							TestUI.getLocalizationService().updateLocations(location);
							location =null;
			        	
			        	}
			        	clean();
						
			        } catch (InvalidValueException e) {
			            Notification.show(e.getMessage());
			            localizationName.setValidationVisible(true);
			 
			            logger.error(e);

			        }
		

			        localizationView.getLocGrid().refreshGrid();
			}
		});
        
        return panel;
    }
	public void refreshLocation(Long  id)
	{
		
		LocationService loalizationService = TestUI.getLocalizationService();
		LocationEntity locationEntity = loalizationService.getLocation(id);
		setLocation(locationEntity);
		getLocalizationName().setValue(locationEntity.getName());
		StringTokenizer st = new StringTokenizer(locationEntity.getAddress(),",");
		getPlace().setValue(st.nextToken());
		getStreet().setValue(st.nextToken().substring(1));


	}
	public void clean()
	{
		localizationName.clear();
    	place.clear();
    	street.clear();		
	}
	public TextField getLocalizationName() {
		return localizationName;
	}


	public LocationEntity getLocation() {
		return location;
	}
	public void setLocation(LocationEntity location) {
		this.location = location;
	}
	public TextField getPlace() {
		return place;
	}
	public TextField getStreet() {
		return street;
	}

	


	
	
}
