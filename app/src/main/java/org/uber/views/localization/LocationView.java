package org.uber.views.localization;

import java.util.Locale;

import org.apache.log4j.Logger;
import org.uber.backend.models.LocationEntity;
import org.uber.components.UberGrid;
import org.uber.views.TestUI;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")

public final class LocationView extends HorizontalLayout implements View {

	private UberGrid<LocationEntity> locGrid;
	private AddNewLocation addLocation;
	@SuppressWarnings("unused")
	private final static Logger logger = Logger.getLogger(LocationView.class);

	public LocationView(){
		setSizeFull();
		VerticalLayout vertical = new VerticalLayout();
		
		addComponent(vertical);
		vertical.addComponent(buildToolbar());

		addLocation = new AddNewLocation(this);
		vertical.addComponent(createGrid().getFullGrid());

		addComponent(addLocation);
		
		Responsive.makeResponsive(this);
		setExpandRatio(vertical, 7);	
		setExpandRatio(addLocation, 3);
	}
	
	private UberGrid<LocationEntity> createGrid(){
		locGrid = new UberGrid<>(TestUI.getLocalizationService(), LocationEntity.class);
		locGrid.getGrid().removeColumn("id");
		locGrid.getGrid().setColumnOrder("name", "address", "latitude", "longitude", "delete");
		locGrid.getGrid().getColumn("name").setHeaderCaption("Nazwa lokalizacji").setHidable(true);
		locGrid.getGrid().getColumn("address").setHeaderCaption("Adres").setHidable(true);
		locGrid.getGrid().getColumn("latitude").setHeaderCaption("Szerokość geograficzna").setHidable(true);
		locGrid.getGrid().getColumn("latitude").setRenderer(new NumberRenderer("%03.7f", Locale.ENGLISH)).setHidable(true);
		locGrid.getGrid().getColumn("longitude").setHeaderCaption("Długość geograficzna").setHidable(true);
		locGrid.getGrid().getColumn("longitude").setRenderer(new NumberRenderer("%03.7f", Locale.ENGLISH)).setHidable(true);
		locGrid.getGrid().getColumn("delete").setHeaderCaption("Usuń").setHidable(true);
		locGrid.setTextFieldInputPrompt("Nazwa lub adres");
		locGrid.setItemClickListener(e->{
			addLocation.refreshLocation(Long.parseLong(e.getItem().getItemProperty("id").toString()));
		});
		return locGrid;
	}

	private Component buildToolbar() {
		HorizontalLayout header = new HorizontalLayout();
		header.addStyleName("viewheader");
		header.setSpacing(true);
		Responsive.makeResponsive(header);

		Label title = new Label("Lista lokalizacji");
		title.setId("ListaLokalizacji");
		title.setSizeUndefined();
		title.addStyleName(ValoTheme.LABEL_H1);
		title.addStyleName(ValoTheme.LABEL_NO_MARGIN);
		header.addComponents(title);

		return header;
	}

	public UberGrid<LocationEntity> getLocGrid() {
		return locGrid;
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		addLocation.clean();
	}
}
