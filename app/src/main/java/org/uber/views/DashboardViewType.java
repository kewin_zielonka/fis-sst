package org.uber.views;

import org.uber.enums.UserRoles;
import org.uber.views.findTaxi.FindTaxi;
import org.uber.views.guest.GuestView;
import org.uber.views.history.HistoryView;
import org.uber.views.localization.LocationView;
import org.uber.views.user.UsersView;

import com.vaadin.navigator.View;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Resource;

public enum DashboardViewType {
	GUEST("Goście", GuestView.class, FontAwesome.SMILE_O, true, "all"), 
	SALES("Znajdź Taxi", FindTaxi.class,FontAwesome.TAXI, false, "all"), 
	HISTORY("Historia", HistoryView.class,FontAwesome.HISTORY, false, UserRoles.MANAGER.getName()), 
	USERMANAGEMENT("Zarządzaj użytkownikami", UsersView.class,FontAwesome.COGS, true,UserRoles.ADMIN.getName()), 
	LOCALIZATIONMANAGEMENT("Zarządzaj lokalizacjami", LocationView.class, FontAwesome.COGS, true, UserRoles.MANAGER.getName());

	private final String viewName;
	private final Class<? extends View> viewClass;
	private final Resource icon;
	private final boolean stateful;
	private final String role;

	private DashboardViewType(final String viewName, final Class<? extends View> viewClass, final Resource icon,
			final boolean stateful, String role) {
		this.viewName = viewName;
		this.viewClass = viewClass;
		this.icon = icon;
		this.stateful = stateful;
		this.role = role;
	}

	public boolean isStateful() {
		return stateful;
	}

	public String getViewName() {
		return viewName;
	}

	public Class<? extends View> getViewClass() {
		return viewClass;
	}

	public Resource getIcon() {
		return icon;
	}

	public static DashboardViewType getByViewName(final String viewName) {
		DashboardViewType result = null;
		for (DashboardViewType viewType : values()) {
			if (viewType.getViewName().equals(viewName)) {
				result = viewType;
				break;
			}
		}
		return result;
	}

	public String getRole() {
		return role;
	}

}
