package org.uber.views;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.picketlink.Identity;
import org.picketlink.Identity.AuthenticationResult;
import org.picketlink.credential.DefaultLoginCredentials;
import org.picketlink.idm.IdentityManager;
import org.picketlink.idm.PartitionManager;
import org.picketlink.idm.RelationshipManager;
import org.picketlink.idm.credential.Password;
import org.picketlink.idm.model.basic.BasicModel;
import org.picketlink.idm.model.basic.Grant;
import org.picketlink.idm.model.basic.Group;
import org.picketlink.idm.model.basic.Role;
import org.picketlink.idm.query.RelationshipQuery;
import org.uber.backend.GuestService;
import org.uber.backend.HistoryService;
import org.uber.backend.LocationService;
import org.uber.backend.UserService;
import org.uber.backend.dao.DaoUber;
import org.uber.backend.dao.implement.DaoUberImpl;
import org.uber.backend.models.User;
import org.uber.enums.UserRoles;

import com.vaadin.annotations.Theme;
import com.vaadin.cdi.CDIUI;
import com.vaadin.server.Page;
import com.vaadin.server.Page.BrowserWindowResizeEvent;
import com.vaadin.server.Page.BrowserWindowResizeListener;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;
import static org.picketlink.idm.model.basic.BasicModel.*;

//@CDIUI(value = "testUI")
@CDIUI("")
@Theme("valo")

@SuppressWarnings("serial")
public class TestUI extends UI {

	final static Logger logger = Logger.getLogger(TestUI.class);

	@Inject
	Identity identity;
	@Inject
	private PartitionManager partitionManager;
	@Inject
	private GuestService guestService;
	@Inject
	private LocationService localizationService;
	@Inject
	private HistoryService historyService;
	@Inject
	private UserService userService;
	@Inject
	private LoginAction loginAction;
	@Inject
	private DefaultLoginCredentials credentials;
	IdentityManager identityManager;
	RelationshipManager relationshipManager;

	@Override
	protected void init(VaadinRequest request) {
		identityManager = this.partitionManager.createIdentityManager();
		relationshipManager = partitionManager.createRelationshipManager();
		DaoUber daoUber = new DaoUberImpl();

		historyService.setUberDao(daoUber);
		localizationService.setUberDao(daoUber);
		create();
		// DashboardEventBus.register(this);
		Responsive.makeResponsive(this);
		addStyleName(ValoTheme.UI_WITH_MENU);

		String code = request.getParameter("code");
		VaadinService.getCurrentRequest().getWrappedSession().setAttribute("code", code);
		updateContent(code);
		// Some views need to be aware of browser resize events so a
		// BrowserResizeEvent gets fired to the event bus on every occasion.
		Page.getCurrent().addBrowserWindowResizeListener(new BrowserWindowResizeListener() {
			@Override
			public void browserWindowResized(final BrowserWindowResizeEvent event) {
				// DashboardEventBus.post(new BrowserResizeEvent());
			}
		});
	}

	/**
	 * Updates the correct content for this UI based on the current user status.
	 * If the user is logged in with appropriate privileges, main view is shown.
	 * Otherwise login view is shown.
	 */
	public void updateContent(String code) {

		User user = (User) VaadinSession.getCurrent().getAttribute(User.class.getName());
		if (user != null) {
			credentials.setUserId(user.getLogin());
			credentials.setPassword(userService.generateMD5(user.getPassword()));
			if (AuthenticationResult.SUCCESS.equals(loginAction.login())) {
				setContent(new MainView());
				removeStyleName("loginview");
				String navidateTo = code != null ? "Znajdź Taxi" : "Goście";
				getNavigator().navigateTo(navidateTo);
			} else {
				setContent(new LoginView());
				addStyleName("loginview");
			}
		} else {
			setContent(new LoginView());
			addStyleName("loginview");
		}
		/*
		 * if (user != null) { User userFromDB =
		 * userService.getUser(user.getLogin()); if (userFromDB != null &&
		 * userFromDB.getPassword().equals(userService.generateMD5(user.
		 * getPassword()))) { setContent(new MainView());
		 * removeStyleName("loginview");
		 * getNavigator().navigateTo(getNavigator().getState()); } else {
		 * setContent(new LoginView()); addStyleName("loginview"); } } else {
		 * 
		 * 
		 * setContent(new LoginView()); addStyleName("loginview"); }
		 */
	}

	public void create() {
		Map<String, Role> rolesMap = new HashedMap();
		Map<String, Role> groupMap = new HashedMap();

		if (getRole(this.identityManager, UserRoles.ADMIN.getName()) == null) {
			Role role = new Role();
			role.setName(UserRoles.ADMIN.getName());
			rolesMap.put(UserRoles.ADMIN.getName(), role);

			Role secretary = new Role();
			secretary.setName(UserRoles.SEKRETARKA.getName());
			secretary.setName(UserRoles.SEKRETARKA.getName());
			rolesMap.put(UserRoles.SEKRETARKA.getName(), secretary);
			Role manager = new Role();
			manager.setName(UserRoles.MANAGER.getName());
			manager.setName(UserRoles.MANAGER.getName());
			rolesMap.put(UserRoles.MANAGER.getName(), manager);
			identityManager.add(role);
			identityManager.add(secretary);
			identityManager.add(manager);

			Group sekretaryGroup = new Group(UserRoles.SEKRETARKA.getName());

			identityManager.add(sekretaryGroup);

			Group managerGroup = new Group(UserRoles.MANAGER.getName(), sekretaryGroup);
			managerGroup.setName(UserRoles.MANAGER.getName());

			identityManager.add(managerGroup);

			identityManager.add(new Group(UserRoles.ADMIN.getName(), managerGroup));

			List<User> usersFromDB = userService.getAllUser();

			for (int i = 0; i < usersFromDB.size(); i++) {
				org.picketlink.idm.model.basic.User user = new org.picketlink.idm.model.basic.User(
						usersFromDB.get(i).getLogin());
				identityManager.add(user);
				identityManager.updateCredential(user, new Password(usersFromDB.get(i).getPassword()));
				// assign global role to user1
				relationshipManager.add(new Grant(user, rolesMap.get(usersFromDB.get(i).getRole())));

				Group group = BasicModel.getGroup(identityManager, usersFromDB.get(i).getRole());
				BasicModel.addToGroup(relationshipManager, user, group);

			}
		}
	}

	public void userLoggedOut() {
		VaadinSession.getCurrent().close();
		identity.logout();
		Page.getCurrent().reload();
	}

	public static GuestService getGuestService() {
		return ((TestUI) getCurrent()).guestService;
	}

	public static HistoryService getHistoryService() {
		return ((TestUI) getCurrent()).historyService;
	}

	public static LocationService getLocalizationService() {
		return ((TestUI) getCurrent()).localizationService;
	}

	public static UserService getUserService() {
		return ((TestUI) getCurrent()).userService;
	}

	public static TestUI getCurrentInstance() {
		return (TestUI) getCurrent();
	}

	public boolean hasApplicationRole(String roleName) {
		if (roleName.equals("all"))
			return true;/*
						 * Role admin = getRole(this.identityManager,
						 * UserRoles.ADMIN.getName());
						 * if(hasRole(this.relationshipManager,
						 * this.identity.getAccount(),admin)) return true; Role
						 * role = getRole(this.identityManager, roleName);
						 * return hasRole(this.relationshipManager,
						 * this.identity.getAccount(), role);
						 */
		Group g = BasicModel.getGroup(identityManager, roleName);
		return BasicModel.isMember(relationshipManager, this.identity.getAccount(), g);
	}

	public void setPartitionManager(PartitionManager partitionManager) {
		this.partitionManager = partitionManager;
	}

}
