package org.uber.views.history;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.uber.backend.models.UberLogModel;
import org.uber.components.UberGrid;
import org.uber.views.TestUI;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")

public final class HistoryView extends HorizontalLayout implements View {

	private final static Logger logger = Logger.getLogger(HistoryView.class);
	
	private UberGrid<UberLogModel> historyGrid;

	public HistoryView() throws UnsupportedOperationException, IOException {
		setSizeFull();
		VerticalLayout vertical = new VerticalLayout();

		addComponent(vertical);
		vertical.addComponent(buildToolbar());
		logger.info("!HistoryView");
		vertical.addComponent(createGrid().getFullGrid());
		
		Responsive.makeResponsive(this);	
	}
	
	private UberGrid<UberLogModel> createGrid(){
		historyGrid = new UberGrid<>(TestUI.getHistoryService(), UberLogModel.class);
		historyGrid.getGrid().removeColumn("id");
		historyGrid.getGrid().removeColumn("delete");
		historyGrid.getGrid().setColumnOrder("guest", "data", "from", "destination", "product","minCost","maxCost","travelTime");
		historyGrid.getGrid().getColumn("guest").setHeaderCaption("Kto").setHidable(true);
		historyGrid.getGrid().getColumn("data").setHeaderCaption("Kiedy").setHidable(true);
		historyGrid.getGrid().getColumn("from").setHeaderCaption("Skąd").setHidable(true);
		historyGrid.getGrid().getColumn("destination").setHeaderCaption("Dokąd").setHidable(true);
		historyGrid.getGrid().getColumn("product").setHeaderCaption("Nazwa usługi").setHidable(true);
		historyGrid.getGrid().getColumn("minCost").setHeaderCaption("Koszt min").setHidable(true);
		historyGrid.getGrid().getColumn("maxCost").setHeaderCaption("Koszt max").setHidable(true);
		historyGrid.getGrid().getColumn("travelTime").setHeaderCaption("Czas dojazdu").setHidable(true);
		historyGrid.setTextFieldInputPrompt("Gość lub lokalizacja");
		return historyGrid;
	}

	private Component buildToolbar() {
		HorizontalLayout header = new HorizontalLayout();
		header.addStyleName("viewheader");
		header.setSpacing(true);
		Responsive.makeResponsive(header);
		Label title = new Label("Historia wyszukań");
		title.setSizeUndefined();
		title.addStyleName(ValoTheme.LABEL_H1);
		title.addStyleName(ValoTheme.LABEL_NO_MARGIN);
		header.addComponents(title);

		return header;
	}

	@Override
	public void enter(ViewChangeEvent event) {
	}
}
