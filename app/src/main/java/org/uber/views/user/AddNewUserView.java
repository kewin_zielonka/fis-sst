package org.uber.views.user;

import org.apache.log4j.Logger;
import org.uber.backend.models.User;
import org.uber.enums.UserRoles;
import org.uber.views.TestUI;

import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")

public final class AddNewUserView extends VerticalLayout{
	private UsersView usersView;
	private TextField login;
	private PasswordField password;
	private ComboBox  role;
	private final static Logger logger = Logger.getLogger(AddNewUserView.class);

	public AddNewUserView(UsersView usersView) {
	 setSizeFull();
	this.usersView = usersView;
	        addStyleName("transactions");

	        addComponent( buildUI());
		Responsive.makeResponsive(this);

	}
	public AddNewUserView() {
	    addComponent( buildUI());	}
	
	private Component buildUI() {
		VerticalLayout panel = new VerticalLayout();
        panel.addStyleName("viewheader");
        panel.setSpacing(true);
        Responsive.makeResponsive(panel);
        panel.setDefaultComponentAlignment(Alignment.BOTTOM_CENTER);
        
        Label title = new Label("Dodaj nowego użytkownika");
        title.setSizeUndefined();        
        panel.addComponent(title);
        
       login = new TextField("Login");
        panel.addComponent(login);
        login.addValidator(new StringLengthValidator(
        	    "Login musi składać się z przynajmniej 2 liter",
        	    2, 25, false));
        
        
        password = new PasswordField("Hasło");
        panel.addComponent(password);
        
        final BeanItemContainer<String> container =
        	    new BeanItemContainer<String>(String.class);

        for (UserRoles p : UserRoles.values())
        {
        	container.addItem(p.getName());
        }

        
        
        role =new ComboBox("Rola", container);
        role.setNullSelectionAllowed(false);
        role.select(UserRoles.SEKRETARKA.getName());
        panel.addComponent(role);
      
        
        login.setValidationVisible(false);

        Button btnSave = new Button("Zapisz",FontAwesome.SAVE);
        panel.addComponent(btnSave);
        btnSave.addClickListener(new ClickListener() {
        	
			@Override
			public void buttonClick(ClickEvent event) {
			        try {
			        	login.validate();
						TestUI.getUserService().saveUser(new User(login.getValue(),password.getValue(),role.getValue().toString()));
						login.clear();
						login.setEnabled(true);
						password.clear();
						role.clear();
						
			        } catch (InvalidValueException e) {
			            Notification.show(e.getMessage());
			            login.setValidationVisible(true);
			 
			            logger.error(e);

			        }
		

			        usersView.getUserGrid().refreshGrid();
			}
		});
        
        return panel;
    }
	public ComboBox getRole() {
		return role;
	}
	public TextField getLogin() {
		return login;
	}

	public PasswordField getPassword() {
		return password;
	}


	
	
}
