package org.uber.views.user;

import org.apache.log4j.Logger;
import org.uber.backend.UserService;
import org.uber.backend.models.User;
import org.uber.components.UberGrid;
import org.uber.views.TestUI;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")

public final class UsersView extends HorizontalLayout implements View {

	private UberGrid<User> userGrid;
	private AddNewUserView addNewUser;
	@SuppressWarnings("unused")
	private final static Logger logger = Logger.getLogger(UsersView.class);

	public UsersView() {
		setSizeFull();
		VerticalLayout vertical = new VerticalLayout();

		addComponent(vertical);
		vertical.addComponent(buildToolbar());

		addNewUser = new AddNewUserView(this);
		vertical.addComponent(createGrid().getFullGrid());
		addComponent(addNewUser);
		setExpandRatio(vertical, 7);
		setExpandRatio(addNewUser, 3);

		Responsive.makeResponsive(this);
	}
	
	private UberGrid<User> createGrid(){
		userGrid = new UberGrid<>(TestUI.getUserService(), User.class);
		userGrid.getGrid().removeColumn("id");
		userGrid.getGrid().setColumnOrder("login", "password", "role", "delete");
		userGrid.getGrid().getColumn("login").setHeaderCaption("Login").setHidable(true);
		userGrid.getGrid().getColumn("password").setHeaderCaption("Hasło").setHidable(true);
		userGrid.getGrid().getColumn("role").setHeaderCaption("Rola").setHidable(true);
		userGrid.getGrid().getColumn("delete").setHeaderCaption("Usuń").setHidable(true);
		userGrid.setTextFieldInputPrompt("Login");
		userGrid.setItemClickListener(e -> {
			refreshUser(e.getItem().getItemProperty("login").getValue().toString());
		});
		return userGrid;
	}

	private void refreshUser(String login) {
		UserService userService = TestUI.getUserService();
		User user = userService.getUser(login);
		addNewUser.getLogin().setValue(user.getLogin());
		addNewUser.getLogin().setEnabled(false);
		addNewUser.getPassword().setValue(user.getPassword());
		addNewUser.getRole().setValue(user.getRole());
	}

	private Component buildToolbar() {
		HorizontalLayout header = new HorizontalLayout();
		header.addStyleName("viewheader");
		header.setSpacing(true);
		Responsive.makeResponsive(header);

		Label title = new Label("Lista użytkowników");
		title.setSizeUndefined();
		title.addStyleName(ValoTheme.LABEL_H1);
		title.addStyleName(ValoTheme.LABEL_NO_MARGIN);
		header.addComponents(title);

		return header;
	}

	public UberGrid<User> getUserGrid() {
		return userGrid;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub

	}
}
