package org.uber.views.guest;

import org.apache.log4j.Logger;
import org.uber.backend.models.Guest;
import org.uber.views.TestUI;

import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")

public final class AddNewGuestView extends VerticalLayout {
	
	private GuestView guestView;
	private String regexp = "[a-zA-Z’'‘ÆÐƎƏƐƔĲŊŒẞÞǷȜæðǝəɛɣĳŋœĸſßþƿȝĄƁÇĐƊĘĦĮƘŁØƠŞȘŢȚŦŲƯY̨Ƴąɓçđɗęħįƙłøơşșţțŧųưy̨ƴÁÀÂÄǍĂĀÃÅǺĄÆǼǢƁĆĊĈČÇĎḌĐƊÐÉÈĖÊËĚĔĒĘẸƎƏƐĠĜǦĞĢƔáàâäǎăāãåǻąæǽǣɓćċĉčçďḍđɗðéèėêëěĕēęẹǝəɛġĝǧğģɣĤḤĦIÍÌİÎÏǏĬĪĨĮỊĲĴĶƘĹĻŁĽĿʼNŃN̈ŇÑŅŊÓÒÔÖǑŎŌÕŐỌØǾƠŒĥḥħıíìiîïǐĭīĩįịĳĵķƙĸĺļłľŀŉńn̈ňñņŋóòôöǒŏōõőọøǿơœŔŘŖŚŜŠŞȘṢẞŤŢṬŦÞÚÙÛÜǓŬŪŨŰŮŲỤƯẂẀŴẄǷÝỲŶŸȲỸƳŹŻŽẒŕřŗſśŝšşșṣßťţṭŧþúùûüǔŭūũűůųụưẃẁŵẅƿýỳŷÿȳỹƴźżžẓ]*";
	private TextField name;
	private TextField lastName;
	private TextField phoneNumber;
	private TextField company;
	private Label title;
	private Guest guest;
	private final static Logger logger = Logger.getLogger(AddNewGuestView.class);

	public AddNewGuestView(GuestView guestView) {
		setSizeFull();
		this.guestView = guestView;
		addStyleName("transactions");
		addComponent(buildUI());
		Responsive.makeResponsive(this);
	}

	public AddNewGuestView() {
		addComponent(buildUI());
	}

	private Component buildUI() {
		VerticalLayout panel = new VerticalLayout();
		panel.addStyleName("viewheader");
		panel.setSpacing(true);
		Responsive.makeResponsive(panel);
		panel.setDefaultComponentAlignment(Alignment.BOTTOM_CENTER);

		title = new Label("Nowy gość");

		title.setSizeUndefined();
		panel.addComponent(title);

		name = new TextField("Imie");
		panel.addComponent(name);
		name.setId("name");
		name.addValidator(new StringLengthValidator("Imie musi składać się z przynajmniej 2 liter", 2, 25, false));
		name.addValidator(new RegexpValidator(regexp, "Imie musi składac się wyłącznie z liter"));

		lastName = new TextField("Nazwisko");
		panel.addComponent(lastName);
		lastName.setId("lastName");
		lastName.addValidator(
				new StringLengthValidator("Nazwisko musi składać się z przynajmniej 2 liter", 2, 25, false));
		lastName.addValidator(new RegexpValidator(regexp, true, "Nazwisko musi składac się wyłącznie z liter"));

		company = new TextField("Firma");
		panel.addComponent(company);
		company.setId("company");
		phoneNumber = new TextField("Nr telefonu");
		panel.addComponent(phoneNumber);
		phoneNumber.setId("phoneNumber");
		phoneNumber.addValidator(new RegexpValidator("[0-9]*", true, "Nr telefonu składa się wyłącznie z cyfr"));

		name.setValidationVisible(false);
		lastName.setValidationVisible(false);
		phoneNumber.setValidationVisible(false);
		setAllFieldsRequired();
		Button btnSave = new Button("Zapisz", FontAwesome.SAVE);
		panel.addComponent(btnSave);
		btnSave.setId("save");
		btnSave.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				try {
					name.validate();
					lastName.validate();
					phoneNumber.validate();
					if (guest == null) {
						TestUI.getGuestService().saveGuest(new Guest(name.getValue(), lastName.getValue(),
								company.getValue(), phoneNumber.getValue()));
					} else {
						guest.setName(name.getValue());
						guest.setLastName(lastName.getValue());
						guest.setCompany(company.getValue());
						guest.setPhone(phoneNumber.getValue());
						TestUI.getGuestService().updateGuest(guest);
						guest = null;
					}
					name.setValidationVisible(false);
					lastName.setValidationVisible(false);
					name.clear();
					lastName.clear();
					company.clear();
					phoneNumber.clear();
					guestView.getGuestGrid().refreshGrid();

				} catch (InvalidValueException e) {
					Notification.show(e.getMessage());
					name.setValidationVisible(true);
					lastName.setValidationVisible(true);
					phoneNumber.setValidationVisible(true);
					logger.error(e);
				}
			}
		});

		return panel;
	}

	public void setAllFieldsRequired() {
		name.setRequired(true);
		name.setRequiredError("Imie jest wymagane!");
		lastName.setRequired(true);
		lastName.setRequiredError("Nazwisko jest wymagane!");      
    }
	
	public void clearAll(){
		name.clear();
		lastName.clear();
		phoneNumber.clear();
		company.clear();
	}

	public TextField getName() {
		return name;
	}

	public void setName(TextField name) {
		this.name = name;
	}

	public TextField getLastName() {
		return lastName;
	}

	public void setLastName(TextField lastName) {
		this.lastName = lastName;
	}

	public TextField getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(TextField phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public TextField getCompany() {
		return company;
	}

	public void setCompany(TextField company) {
		this.company = company;
	}

	public void setTitle(Label title) {
		this.title = title;
	}

	public Label getTitle() {
		return title;
	}

	public Guest getGuest() {
		return guest;
	}

	public void setGuest(Guest guest) {
		this.guest = guest;
	}
}
