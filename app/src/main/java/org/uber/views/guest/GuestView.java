package org.uber.views.guest;

import org.apache.log4j.Logger;
import org.uber.backend.models.Guest;
import org.uber.components.UberGrid;
import org.uber.views.TestUI;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")

public final class GuestView extends HorizontalLayout implements View {
	private AddNewGuestView addNewGuest;
	private UberGrid<Guest> guestGrid;
	@SuppressWarnings("unused")
	private final static Logger logger = Logger.getLogger(GuestView.class);

	public GuestView() {	
		setSizeFull();
		VerticalLayout vertical = new VerticalLayout();
		addComponent(vertical);
		vertical.addComponent(buildToolbar());
		addNewGuest = new AddNewGuestView(this);
		vertical.addComponent(createGrid().getFullGrid());
		vertical.setWidth("100%");
		Responsive.makeResponsive(this);
	}
	
	private UberGrid<Guest> createGrid(){
		guestGrid = new UberGrid<>(TestUI.getGuestService(), Guest.class);
		guestGrid.getGrid().removeColumn("id");
		guestGrid.getGrid().setColumnOrder("lastName", "name", "company", "phone", "delete");
		guestGrid.getGrid().getColumn("name").setHeaderCaption("Imię").setHidable(true);
		guestGrid.getGrid().getColumn("lastName").setHeaderCaption("Nazwisko").setHidable(true);
		guestGrid.getGrid().getColumn("company").setHeaderCaption("Firma").setHidable(true);
		guestGrid.getGrid().getColumn("phone").setHeaderCaption("Numer telefonu").setHidable(true);
		guestGrid.getGrid().getColumn("delete").setHeaderCaption("Usuń gościa").setHidable(true);
		guestGrid.setTextFieldInputPrompt("Imię lub nazwisko");

		if (!TestUI.getCurrentInstance().hasApplicationRole("administrator")) {
			guestGrid.getGrid().removeColumn("delete");
		} else {
			guestGrid.setItemClickListener(event -> {
				Long selectedId = Long.parseLong(event.getItem().getItemProperty("id").toString());
				if (addNewGuest.isVisible() && addNewGuest.getTitle().getValue().equals("Edytowany gość")) {
					addNewGuest.setVisible(true);
					addNewGuest.clearAll();
				} else {
					addNewGuest.getTitle().setValue("Edytowany gość");
					addNewGuest.setVisible(true);
				}
				refreshGuest(selectedId);
			});
		}
		return guestGrid;
	}

	private Button createAddButton() {
		Button button = new Button("Dodaj nowego gościa", e -> {
			if (getComponentIndex(addNewGuest) != -1) {
				addNewGuest.clearAll();
				removeComponent(addNewGuest);
			} 
			else {
				addNewGuest.getTitle().setValue("Nowy gość");
				addComponent(addNewGuest);
				addNewGuest.setVisible(true);
				addNewGuest.clearAll();
			}
		});
		button.setIcon(FontAwesome.PLUS);
		button.setId("addGuest");

		return button;
	}

	private void refreshGuest(Long id) {
		Guest guest = TestUI.getGuestService().getGuestById(id);
		addNewGuest.setGuest(guest);
		addNewGuest.getName().setValue(guest.getName());
		addNewGuest.getLastName().setValue(guest.getLastName());
		addNewGuest.getCompany().setValue(guest.getCompany());
		addNewGuest.getPhoneNumber().setValue(guest.getPhone());
	}

	private Component buildToolbar() {
		HorizontalLayout header = new HorizontalLayout();
		header.addStyleName("viewheader");
		header.setSpacing(true);
		Responsive.makeResponsive(header);
		Label title = new Label("Lista Gości");
		title.setId("Goscie");
		title.setSizeUndefined();
		title.addStyleName(ValoTheme.LABEL_H1);
		title.addStyleName(ValoTheme.LABEL_NO_MARGIN);
		header.addComponents(title);
		if (TestUI.getCurrentInstance().hasApplicationRole("administrator"))
			header.addComponent(createAddButton());
		return header;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub

	}

	public UberGrid<Guest> getGuestGrid() {
		return guestGrid;
	}
	
}
