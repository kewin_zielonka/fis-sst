package org.example;

import com.vaadin.annotations.Theme;
import com.vaadin.cdi.CDIUI;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

import java.util.List;

import javax.inject.Inject;

import org.uber.backend.dao.DaoGuest;
import org.uber.backend.dao.DaoUser;
import org.uber.backend.models.Guest;
import org.uber.backend.models.User;

@CDIUI(value = "test")
@Theme("valo")
public class TestUI extends UI {

	private static final long serialVersionUID = -1317135531957741415L;
	
	@Inject
    DaoGuest guest;
	
	@Inject
    DaoUser user;

    @Override
    protected void init(VaadinRequest request) {
    	
		List<Guest> list  = guest.getAllGuests();
		
		User findUser = user.getUser("kzielonka");

    }

}