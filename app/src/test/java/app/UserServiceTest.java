package app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;
import org.uber.backend.UserService;
import org.uber.backend.models.User;

public class UserServiceTest {
	static UserService service;


	@BeforeClass
	public static void before() {

		service = new UserService();
	}

	@Test
	public void md5GeneratorTest() {

		User user = new User("Tomasz","nowak","administrator");
		String password=service.generateMD5(user.getPassword());

		assertNotNull("Works",password);
		assertEquals("160af033fb05136b11ec2f9489704394", password);

	}


	@Test
	public void testGetUsers() {
		User user = service.getUser("");

		assertTrue("Error while getting User Object", user == null);

	}


}
