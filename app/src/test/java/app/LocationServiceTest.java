package app;


import static org.junit.Assert.*;

import java.util.List;
import org.apache.log4j.Logger;

import org.junit.BeforeClass;
import org.junit.Test;
import org.uber.backend.LocationService;
import org.uber.backend.dao.implement.DaoUberImpl;
import org.uber.backend.models.LocationEntity;

public class LocationServiceTest {
	static LocationService service;
	private final static Logger logger = Logger.getLogger(LocationServiceTest.class);
	@BeforeClass
	public static void before() {

		service = new LocationService();
		
		service.setUberDao(new DaoUberImpl());
	}

	@Test
	public void changeAdressToCoordinate() throws Exception {
		LocationEntity l = new LocationEntity("Test", "Bojkowska 37c gliwice");
		service.addresToCoordinate(l);
		assertEquals(50.2793292, l.getLatitude(),0.0000001);

	}
	@Test
	public void getLocatoins() {
		List<LocationEntity> locations = service.getLocationList();
		logger.info(locations.toString());
		assertFalse("Error, there are nothing in Location List", locations.isEmpty());
	}



}
