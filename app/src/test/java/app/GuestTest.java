package app;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.uber.backend.dao.DaoGuest;
import org.uber.backend.dao.DaoUser;
import org.uber.backend.dao.implement.DaoGuestImpl;
import org.uber.backend.dao.implement.DaoUserImpl;
import org.uber.backend.models.Guest;
import org.uber.backend.models.User;
import org.uber.views.TestUI;
import org.uber.views.guest.AddNewGuestView;
import org.uber.views.guest.GuestView;

import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.ui.Notification;

public class GuestTest {


	static AddNewGuestView addNewGuestView ;
	private final static Logger logger = Logger.getLogger(GuestTest.class);
	
	@BeforeClass
	public static void before() {
		addNewGuestView = new AddNewGuestView();
	}

	@Test
	public void guestNameValidationFail() {
		addNewGuestView.getName().setValue("Ad7m");
		 try {
				addNewGuestView.getName().validate();
				
	        } catch (InvalidValueException e) {
	        	assertEquals( "Imie musi składac się wyłącznie z liter",e.getMessage().toString());
	        	logger.error(e);
	        }

	}
	@Test
	public void guestLastNameValidation() {
		addNewGuestView.getLastName().setValue("No!wak");
		 try {
				addNewGuestView.getLastName().validate();
				
	        } catch (InvalidValueException e) {
	        	assertEquals( "Nazwisko musi składac się wyłącznie z liter",e.getMessage().toString());
	        	logger.error(e);
	           
	        }

	}
	@Test
	public void guestPhoneNrValidation() {
		addNewGuestView.getPhoneNumber().setValue("+48112");
		 try {
				addNewGuestView.getPhoneNumber().validate();
				
	        } catch (InvalidValueException e) {
	        	assertEquals( "Nr telefonu składa się wyłącznie z cyfr",e.getMessage().toString());
	        	logger.error(e);
	        }

	}
}
