package app;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.junit.BeforeClass;
import org.junit.Test;
import org.uber.backend.dao.DaoGuest;
import org.uber.backend.dao.DaoUser;
import org.uber.backend.dao.implement.DaoGuestImpl;
import org.uber.backend.dao.implement.DaoUserImpl;
import org.uber.backend.models.Guest;
import org.uber.backend.models.User;

public class DaoTest {

	private static DaoGuest daoGuest;
	private static DaoUser daoUser;
	private static EntityManagerFactory entityManagerFactory;
	private static EntityManager entityManager;

	@BeforeClass
	public static void before() {
		entityManagerFactory = Persistence.createEntityManagerFactory("persistence");
		entityManager = entityManagerFactory.createEntityManager();
		daoGuest = new DaoGuestImpl();
		daoUser = new DaoUserImpl();
	}

	 @Test
	 public void testGetAllGuests() {

	 CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	 CriteriaQuery<Long> query = cb.createQuery(Long.class);
	 Root<Guest> root = query.from(Guest.class);

	 query.select(cb.count(root));
	 long count = entityManager.createQuery(query).getSingleResult();
	 List<Guest> testList = daoGuest.getAllGuests();

	 assertTrue("Error, sizeOf Guest list does not match. \nCount: "+count+"\nListSize: "+testList.size(), (long)testList.size() == count);

	 }
	 
	 @Test
	 public void testGetAllUsers() {

	 CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	 CriteriaQuery<Long> query = cb.createQuery(Long.class);
	 Root<User> root = query.from(User.class);

	 query.select(cb.count(root));
	 long count = entityManager.createQuery(query).getSingleResult();

	 List<User> testList = daoUser.getAllUsers();

	 assertTrue("Error, sizeOf Guest list does not match. \nCount: "+count+"\nListSize: "+testList.size(), (long)testList.size() == count);

	 }

	@Test
	public void testGetUsers() {
		User user = daoUser.getUser("");

		assertTrue("Error while getting Guest Object", user == null);

	}
	
	@Test 
	public void testGetGuestById(){
		List<Guest> guestList = daoGuest.getAllGuests();
		for(Guest guest : guestList){		
			Guest newGuest = daoGuest.getGuest(guest.getId());			
			assertNotNull("Error while getting guest by id,guest is null", newGuest);
			assertEquals("Guests object aren't equal", guest, newGuest);
		}
	}
	
	@Test
	public void countGuestsTest(){
		int value = daoGuest.countGuests();
		System.out.println("Count returned: "+value);
		assertNotNull("Method returned null",value);
	}
	
	@Test
	public void testGetLimitedGuestsList(){
		int first = 3, max = 10, listSize=0;
		int value = daoGuest.countGuests();
		List<Guest> list = daoGuest.getLimitedGuestsList(first, max);
		
		for(Guest guest : list){
			System.out.println(guest);
		}
		
		assertNotNull("Returned list is null",list);
		
		if(value-first<max)
			listSize=value-first;
		else listSize = max;
		
		assertTrue("Returned list size is differs as expected",list.size()==listSize);
	}
	
	@Test
	public void searchGuestTest(){
		String name = "de";
		String lastName = "wie";
		List<Guest> nameList = daoGuest.searchGuestByName(name);
		List<Guest> lastNameList = daoGuest.searchGuestByLastName(lastName);
		Set<Guest> resultSet = new HashSet<>();
		
		for(Guest guest : lastNameList){
			System.out.println("LastName: " + guest);
		}
		
		for(Guest guest : nameList){
			System.out.println("Name: "+ guest);
		}
		
		resultSet.addAll(nameList);
		resultSet.addAll(lastNameList);
		
		for(Guest guest : resultSet){
			System.out.println("Set: "+ guest);
		}
		
		assertFalse("Empty set!",resultSet.isEmpty());
		assertFalse("Empty lastName list!",lastNameList.isEmpty());
		assertFalse("Empty name list!",nameList.isEmpty());
	}
}
