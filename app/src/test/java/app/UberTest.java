package app;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.uber.backend.dao.DaoUber;
import org.uber.backend.dao.implement.DaoUberImpl;
import org.uber.backend.models.LocationEntity;
import org.uber.backend.models.UberListModel;
import org.uber.backend.models.UberLogModel;

import com.uber.sdk.rides.client.model.PriceEstimatesResponse;
import com.uber.sdk.rides.client.model.Product;
import com.uber.sdk.rides.client.model.ProductsResponse;

public class UberTest {

	private static DaoUber daoUber;
	private final static Logger logger = Logger.getLogger(UberTest.class);

	@BeforeClass
	public static void before() {
		daoUber = new DaoUberImpl();
	}

	@Test
	public void testFindUber() {
		ProductsResponse products;
		try {
			logger.info("Getting uber");
			products = daoUber.findUber(52.518509f, 13.407441f);
			logger.debug(products.getProducts());
			assertTrue("Error, UberResultModel is null", products != null);
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Catched exception in  testFindUber: " + e);
		}

	}

	@Test
	public void estimateTime() {
		long estimatedTime;
		ProductsResponse products;
		try {
			products = daoUber.findUber(52.236044f, 21.019213f);

			for (Product x : products.getProducts()) {
				estimatedTime = daoUber.estimateTime(52.236044f, 21.019213f, x.getProductId());
				logger.info("Estimated time: " + estimatedTime + " for " + x.getDisplayName());
				assertTrue("Error, estimated time does not match", estimatedTime != 0);
			}
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Catched exception: " + e);
		}
	}

	@Test
	public void getLocatoins() {
		List<LocationEntity> locations = daoUber.getLocations();
		logger.info(locations.toString());
		assertTrue("Error, there are nothing in Location List", !locations.isEmpty());
	}

	@Test
	public void estimatePrice() {
		PriceEstimatesResponse estimatedPrice;
		try {
			estimatedPrice = daoUber.estimatePrice(52.236044f, 21.019213f, 52.167447f, 20.967859f);
			logger.info("Estimated price: " + estimatedPrice);
			assertTrue("Error, estimated price does not match", estimatedPrice != null);
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Catched exception: " + e);
		}
	}

	@Test
	public void getUberSearchList() {

		try {
			List<UberListModel> list = daoUber.getUberList(52.236044f, 21.019213f, 52.167447f, 20.967859f);
			assertTrue("Error, returned empty list in getUberSearch", !list.isEmpty());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Catched exception: " + e);
		}

	}

	@Test
	public void getLogs() {
		List<UberLogModel> list;
		try {
			list = daoUber.getLogList();
			logger.info(list.toString());
			assertTrue("Error, there are nothing in Location List", !list.isEmpty());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Catched exception: " + e);
		}
	}
	
	@Test
	public void requestUberGetUrl(){
		
		String response = null;
		try {
			response = daoUber.getRedirectUrl(50.2793292f,18.6854823f,50.288706f,18.677266f,"0a9bd344-f8b8-436b-8115-b2bfe1900997");
		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.info(response);
		
	}
	
	@Test
	public void getUberListTest(){
		
		List<UberListModel> list;
		try {
			list = daoUber.getUberList("Siedziba firmy", "Hotel Ibis", "Anonymous User");
			logger.info(list.toString());
			assertTrue("Error, there are nothing in Location List", !list.isEmpty());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Catched exception: " + e);
		}
	}
	
}
