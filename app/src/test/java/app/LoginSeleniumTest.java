package app;
import static org.junit.Assert.*;
/*
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.persistence.Persistence;
import javax.validation.constraints.AssertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.uber.backend.dao.DaoGuest;
import org.uber.backend.dao.DaoUber;
import org.uber.backend.dao.implement.DaoGuestImpl;
import org.uber.backend.dao.implement.DaoUberImpl;
import org.uber.backend.models.Guest;
import org.uber.backend.models.LocationEntity;

import com.thoughtworks.selenium.Selenium;
public class LoginSeleniumTest {

	static WebDriver driver;
	static DaoGuest guestTest;
	static DaoUber location;


	@BeforeClass
	public static void before()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");

		driver =new ChromeDriver();	
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://localhost:8080/app/");
		driver.manage().window().maximize();
}


	@Test
	public void loginUser() throws InterruptedException{

//Logowanie
		driver.findElement(By.id("username")).sendKeys("admin"); //wpisuje login
		driver.findElement(By.id("password")).sendKeys("admin");//wpisuje haslo
		driver.findElement(By.id("singin")).click();

		assertTrue("Błąd logowania",driver.findElement(By.id("dashboardmenu")).isDisplayed()); //Sprawdź czy zalogow
//Dodawanie nowego gościa
		driver.findElement(By.id("addGuest")).click();
		assertTrue("Błąd otwierania widoku dodawania gościa",driver.findElement(By.id("name")).isDisplayed()); 
		Guest g = new Guest("Dietmar","Kuroczik","FIS-SST","5874125889");
		driver.findElement(By.id("name")).sendKeys(g.getName()); 
		driver.findElement(By.id("lastName")).sendKeys(g.getLastName());
		driver.findElement(By.id("company")).sendKeys(g.getCompany());
		driver.findElement(By.id("phoneNumber")).sendKeys(g.getPhone());
		guestTest = new DaoGuestImpl();

		driver.findElement(By.id("save")).click();
		guestTest = new DaoGuestImpl();
		int countafter = guestTest.getAllGuests().size();
//sprawdzanie czy znajduje sie w tabeli
		WebElement table_element = driver.findElement(By.id("table"));
		WebElement lastName=table_element.findElement(By.xpath("id('table')//tr["+countafter+"]/td[1]"));
		WebElement name=table_element.findElement(By.xpath("id('table')//tr["+countafter+"]/td[2]"));


		assertEquals("Gość nie został dodany do tabelki",g.getLastName()+g.getName(), lastName.getText()+name.getText());
//Przejście do widoku lokalizacji i doadanie nowej lokalizacji
		driver.findElement(By.xpath("//*[text()[contains(.,'Zarządzaj lokalizacjami')]]")).click();
		WebElement myDynamicElement = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("ListaLokalizacji")));
		assertTrue("Błąd otwierania widoku lokalizacji",driver.findElement(By.id("ListaLokalizacji")).isDisplayed()); 
		LocationEntity locationEntity = new LocationEntity("Minibrowar Majer","Gliwice, Studzienna 8");

		driver.findElement(By.id("name")).sendKeys(locationEntity.getName()); 
		driver.findElement(By.id("city")).sendKeys("Gliwice");
		driver.findElement(By.id("street")).sendKeys( "Studzienna 8");

		driver.findElement(By.id("save")).click();

		WebElement myDynamicElement1 = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[text()[contains(.,'50.2958')]]")));
		WebElement table_elementLocation = driver.findElement(By.id("table"));
		location = new DaoUberImpl();
		int countLocation = location.getLocations().size();
		WebElement locationName=table_elementLocation.findElement(By.xpath("id('table')//tr["+countLocation+"]/td[1]"));
		assertEquals("Adres nie został dodany do tabelki",locationEntity.getName(),locationName.getText());
		
//Przejscie do widkoku szukania 
		driver.findElement(By.xpath("//*[text()[contains(.,'Znajdź Taxi')]]")).click();
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[text()[contains(.,'Znajdź Taxi')]]")));

		WebElement comboGuest = driver.findElement(By.xpath("//div[@id='app-96801']/div/div[2]/div/div[2]/div/div/div/div/div/div[2]/div/table/tbody/tr[1]/td[3]/div/div"));
		comboGuest.click();
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='VAADIN_COMBOBOX_OPTIONLIST']/div/div[2]/table/tbody/tr["+(countafter+1)+"]/td/span")));

		driver.findElement(By.xpath("//div[@id='VAADIN_COMBOBOX_OPTIONLIST']/div/div[2]/table/tbody/tr["+(countafter+1)+"]/td/span")).click();

		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='app-96801']/div/div[2]/div/div[2]/div/div/div/div/div/div[2]/div/table/tbody/tr[2]/td[3]/div/div")));

		
		driver.findElement(By.xpath("//div[@id='app-96801']/div/div[2]/div/div[2]/div/div/div/div/div/div[2]/div/table/tbody/tr[2]/td[3]/div/div")).click();
		Thread.sleep(500);

		driver.findElement(By.xpath("//div[@id='VAADIN_COMBOBOX_OPTIONLIST']/div/div[2]/table/tbody/tr["+(countLocation+1)+"]/td/span")).click();
	//	driver.findElement(By.xpath("//div[@id='VAADIN_COMBOBOX_OPTIONLIST']/div/div[2]/table/tbody/tr["+(countLocation+1)+"]/td/span")).click();
		Thread.sleep(500);
		WebElement myDynamicElement3 = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='app-96801']/div/div[2]")));
		
		driver.findElement(By.xpath("//div[@id='app-96801']/div/div[2]/div/div[2]/div/div/div/div/div/div[2]/div/table/tbody/tr[3]/td[3]/div/div")).click();
		driver.findElement(By.xpath("//div[@id='VAADIN_COMBOBOX_OPTIONLIST']/div/div[2]/table/tbody/tr[6]/td/span")).click();
		driver.findElement(By.xpath("//div[@id='VAADIN_COMBOBOX_OPTIONLIST']/div/div[2]/table/tbody/tr[6]/td/span")).click();

		WebElement myDynamicElement5 = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(By.id("serch")));
		driver.findElement(By.id("serch")).click();

		int rowCount=driver.findElements(By.xpath("//div[@id='app-96801']/div/div[2]/div/div[2]/div/div/div/div/div/div[3]/div/div/div/div/table/tbody/tr")).size();		
		assertTrue("Nie znaleziono ubera", 1 <rowCount);
//Przejscie do histori oras sprawdzenie czy znajduje sie tam nasze wyszukanie
		driver.findElement(By.xpath("//*[text()[contains(.,'Historia')]]")).click();
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[text()[contains(.,'Historia wyszukań')]]")));

		String historyGuestName= driver.findElement(By.xpath("(//div[@id='table']/div[3]/table/tbody/tr[1]/td)[1]")).getText();
		String histotyFrom= driver.findElement(By.xpath("(//div[@id='table']/div[3]/table/tbody/tr[1]/td)[3]")).getText();
		assertEquals("Przejazd nie został wyszukany",g.getName()+" "+g.getLastName()+locationEntity.getName(),historyGuestName+histotyFrom);
		
		//Przejscie do gosci i usunięcie goscia
		driver.findElement(By.xpath("//*[text()[contains(.,'Goście')]]")).click();
		(new WebDriverWait(driver, 10))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("Goscie")));

		driver.findElement(By.xpath("(//button[@type='button'])["+countafter+"]")).click();

		guestTest = new DaoGuestImpl();
		int countafterDelete = guestTest.getAllGuests().size();

		assertTrue("Gość nie został usuniety"+ (countafter-1)+"  "+countafterDelete, countafter-1 == countafterDelete);

//Przejscie do lokalizacji i usuniecie lokalizacji
		driver.findElement(By.xpath("//*[text()[contains(.,'Zarządzaj lokalizacjami')]]")).click();
		WebElement myDynamicElement4 = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("ListaLokalizacji")));
		assertTrue("Błąd otwierania widoku lokalizacji",driver.findElement(By.id("ListaLokalizacji")).isDisplayed()); 

		driver.findElement(By.xpath("(//button[@type='button'])["+countLocation+"]")).click();

		location = new DaoUberImpl();
		int countLocationafterDelete = location.getLocations().size();
		assertTrue("Gość nie został usuniety"+ (countLocation-1)+"  "+countLocationafterDelete, countLocation-1 == countLocationafterDelete);

	}
	@AfterClass
	public static void after()
	{
		driver.close();
	}
	
}
*/